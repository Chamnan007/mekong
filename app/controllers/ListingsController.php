<?php
class ListingsController extends BaseController{

	private $limit = 6;
		
	public function anyIndex(){
		
		$dataProperty = array();
 		$room = array();
 		$datacategoryType = array();
 		$location    = array();
 		$states      = array();

 		$page 		 = Input::get('page',1);
 		$pro_rent_sale = Input::get('pro_rent_sale', null);
 		$province_id = Input::get('province_id', null);
 		$pro_category_type = Input::get('pro_category_type', null);
 		$pro_category      = Input::get('pro_category', null);

 		$bedroom 	 = Input::get('bedroom', null);
 		$bathroom    = Input::get('bathroom', null);
 		$min_price   = Input::get('min_price', null);
 		$max_price   = Input::get('max_price', null);

 		$strToken = Helper::buildTokenURL();

 	 	try 
		 {
			$message = array(
						'success'=>true,
						'message'=>''
					);
				$param = array(
					'branch_id'=>Config::get('global.branch_id',4),
					'per_page' => $this->limit,
					'sort'     => 'created_at',
					'page'	  => $page,
					'dir'      => 'DESC'
				);
				if(!is_null($pro_rent_sale))
				{
					$param['type'] = $pro_rent_sale;
					$this->setState('search.type',$pro_rent_sale);
				}
				if(!is_null($province_id))
				{
					$param['province_id'] = $province_id;
					$this->setState('search.province_id', $province_id);
				}
				if(!is_null($pro_category_type))
				{
					$param['category'] = $pro_category_type;
					$this->setState('search.pro_category_type', $pro_category_type);
				}
				if(!is_null($pro_category))
				{
					$param['residential_type'] = $pro_category;
					$this->setState('search.pro_category', $pro_category);
				}
				if(!is_null($bedroom))
				{
					$param['bedroom'] = $bedroom;
					$this->setState('search.bedroom', $bedroom);
				}
				if(!is_null($bathroom))
				{
					$param['bathroom'] = $bathroom;
					$this->setState('search.bathroom', $bathroom);
				}
				if(!is_null($min_price))
				{
					$param['from_price'] = $min_price;
					$this->setState('search.min_price', $min_price);
				}
				if(!is_null($max_price))
				{
					$param['to_price'] = $max_price;
					$this->setState('search.max_price', $max_price);
				}

				// get property
				$strParm = Helper::buildParam($param);
				$url 	= Config::get('global.base_api_url');
				$url 	.=	'property?';
				$url 	.=  $strParm;	
			    $property = RestClient::get($url)->getContent();
				$dataProperty = json_decode($property,true);
				
				// get rooms
				$room = Config::get('global.room');

				// get category types
				$url 	= Config::get('global.base_api_url');
				$url 	.=	'category';
				$url 	.= '?' . $strToken;
				$categoryType = RestClient::get($url)->getContent();
				$datacategoryType = json_decode($categoryType,true);
				
				// get locations
				$country_id = Config::get('global.country_cambodia_id', 1);
				$url 	= Config::get('global.base_api_url');
				$url 	.=	'province?country_id='.$country_id;
				$url 	.= '&' . $strToken;
				$location = RestClient::get($url)->getContent();
				$location = json_decode($location,true);

				$states = $this->states(); 
		
		 } catch (Exception $e) {

		    $message['success'] = false;
		 	$message['message'] = $e->getMessage();
			Session::flash('message', $message);
		}
			
		return View::make('listing.index',array(
				'dataProperty' 	  	 => $dataProperty,
				'room'  => $room,
				'datacategoryType'    => $datacategoryType,
				'location'		  => $location,
				'states'		  => $states
			));
	}
	public function getDetail($id = 0)
	{
		$dataPropertyDetail = array();
 		$dataPropertyFeature = array();

 		$room = array();
 		$datacategoryType = array();
 		$location    = array();

 		$strToken = Helper::buildTokenURL();

 		try 
		{
			if($id)
			{
				$message = array(
						'success'=>true,
						'message'=>''
					);
				$param = array(
					'branch_id'=>Config::get('global.branch_id',4),
					'property_id'	   => $id	
				);

				// get property detail
				$strParm = Helper::buildParam($param);
				$url 	= Config::get('global.base_api_url');
				$url 	.=	'property?';
				$url 	.=  $strParm;		
			    $propertyDetail = RestClient::get($url)->getContent();
				$dataPropertyDetail = json_decode($propertyDetail,true);

				$property = array();
                if(isset($dataPropertyDetail['data'][0]['property']))
                {
                    $property = $dataPropertyDetail['data'][0]['property'];
                  
                }
                else
                {
                   	return View::make('page.notfound');
                }

                $galleries = array();
                if(isset($dataPropertyDetail['data'][0]['gallery_images']))
                {
                    $galleries = $dataPropertyDetail['data'][0]['gallery_images'];
                }

                $featured = array();
                if(isset($dataPropertyDetail['data'][0]['features']))
                {
                    $featured = $dataPropertyDetail['data'][0]['features'];
                }

                $similars = array();
                if(isset($dataPropertyDetail['data'][0]['related_properties']))
                {
                    $similars = $dataPropertyDetail['data'][0]['related_properties'];
                }

                // get rooms
				$room = Config::get('global.room');

				// get category types
				$url 	= Config::get('global.base_api_url');
				$url 	.=	'category';
				$url 	.= '?' . $strToken;
				$categoryType = RestClient::get($url)->getContent();
				$datacategoryType = json_decode($categoryType,true);	
				
				// get locations
				$country_id = Config::get('global.country_cambodia_id', 1);
				$url 	= Config::get('global.base_api_url');
				$url 	.=	'province?country_id='.$country_id;
				$url 	.= '&' . $strToken;
				$location = RestClient::get($url)->getContent();
				$location = json_decode($location,true);
			}	
			else 
			{
				
			}	

		} catch (Exception $e) {

		    $message['success'] = false;
			$message['message'] = $e->getMessage();
			Session::flash('message', $message);
		}
			
		return View::make('listing.detail',array(
				'property' 	  => $property,
				'galleries'   => $galleries,
				'similars'    => $similars,
				'featured'    => $featured,
				'room'  	  => $room,
				'datacategoryType'    => $datacategoryType,
				'location'		  => $location,
			));
	}

	// for rent properties
	public function anyRent(){
		
		$dataProperty = array();
 		$room = array();
 		$datacategoryType = array();
 		$location    = array();
 		$states      = array();

 		$page 		 = Input::get('page',1);
 		$pro_rent_sale = 0;
 		$province_id = Input::get('province_id', null);
 		$pro_category_type = Input::get('pro_category_type', null);
 		$pro_category      = Input::get('pro_category', null);

 		$bedroom 	 = Input::get('bedroom', null);
 		$bathroom    = Input::get('bathroom', null);
 		$min_price   = Input::get('min_price', null);
 		$max_price   = Input::get('max_price', null);

 		$strToken = Helper::buildTokenURL();

 	 	try 
		 {
			$message = array(
						'success'=>true,
						'message'=>''
					);
				$param = array(
					'branch_id'=>Config::get('global.branch_id',4),
					'per_page' => $this->limit,
					'sort'     => 'created_at',
					'page'	  => $page,
					'dir'      => 'DESC'
				);
				if(!is_null($pro_rent_sale))
				{
					$param['type'] = $pro_rent_sale;
					$this->setState('search.type',$pro_rent_sale);
				}
				if(!is_null($province_id))
				{
					$param['province_id'] = $province_id;
					$this->setState('search.province_id', $province_id);
				}
				if(!is_null($pro_category_type))
				{
					$param['category'] = $pro_category_type;
					$this->setState('search.pro_category_type', $pro_category_type);
				}
				if(!is_null($pro_category))
				{
					$param['residential_type'] = $pro_category;
					$this->setState('search.pro_category', $pro_category);
				}
				if(!is_null($bedroom))
				{
					$param['bedroom'] = $bedroom;
					$this->setState('search.bedroom', $bedroom);
				}
				if(!is_null($bathroom))
				{
					$param['bathroom'] = $bathroom;
					$this->setState('search.bathroom', $bathroom);
				}
				if(!is_null($min_price))
				{
					$param['from_price'] = $min_price;
					$this->setState('search.min_price', $min_price);
				}
				if(!is_null($max_price))
				{
					$param['to_price'] = $max_price;
					$this->setState('search.max_price', $max_price);
				}

				// get property
				$strParm = Helper::buildParam($param);
				$url 	= Config::get('global.base_api_url');
				$url 	.=	'property?';
				$url 	.=  $strParm;	
			    $property = RestClient::get($url)->getContent();
				$dataProperty = json_decode($property,true);
				
				// get rooms
				$room = Config::get('global.room');

				// get category types
				$url 	= Config::get('global.base_api_url');
				$url 	.=	'category';
				$url 	.= '?' . $strToken;
				$categoryType = RestClient::get($url)->getContent();
				$datacategoryType = json_decode($categoryType,true);
				
				// get locations
				$country_id = Config::get('global.country_cambodia_id', 1);
				$url 	= Config::get('global.base_api_url');
				$url 	.=	'province?country_id='.$country_id;
				$url 	.= '&' . $strToken;
				$location = RestClient::get($url)->getContent();
				$location = json_decode($location,true);

				$states = $this->states(); 
		
		 } catch (Exception $e) {

		    $message['success'] = false;
		 	$message['message'] = $e->getMessage();
			Session::flash('message', $message);
		}
			
		return View::make('listing.index',array(
				'dataProperty' 	  	 => $dataProperty,
				'room'  => $room,
				'datacategoryType'    => $datacategoryType,
				'location'		  => $location,
				'states'		  => $states
			));
	}

	// for rent properties
	public function anySale(){
		
		$dataProperty = array();
 		$room = array();
 		$datacategoryType = array();
 		$location    = array();
 		$states      = array();

 		$page 		 = Input::get('page',1);
 		$pro_rent_sale = 1;
 		$province_id = Input::get('province_id', null);
 		$pro_category_type = Input::get('pro_category_type', null);
 		$pro_category      = Input::get('pro_category', null);

 		$bedroom 	 = Input::get('bedroom', null);
 		$bathroom    = Input::get('bathroom', null);
 		$min_price   = Input::get('min_price', null);
 		$max_price   = Input::get('max_price', null);

 		$strToken = Helper::buildTokenURL();

 	 	try 
		 {
			$message = array(
						'success'=>true,
						'message'=>''
					);
				$param = array(
					'branch_id'=>Config::get('global.branch_id',4),
					'per_page' => $this->limit,
					'sort'     => 'created_at',
					'page'	  => $page,
					'dir'      => 'DESC'
				);
				if(!is_null($pro_rent_sale))
				{
					$param['type'] = $pro_rent_sale;
					$this->setState('search.type',$pro_rent_sale);
				}
				if(!is_null($province_id))
				{
					$param['province_id'] = $province_id;
					$this->setState('search.province_id', $province_id);
				}
				if(!is_null($pro_category_type))
				{
					$param['category'] = $pro_category_type;
					$this->setState('search.pro_category_type', $pro_category_type);
				}
				if(!is_null($pro_category))
				{
					$param['residential_type'] = $pro_category;
					$this->setState('search.pro_category', $pro_category);
				}
				if(!is_null($bedroom))
				{
					$param['bedroom'] = $bedroom;
					$this->setState('search.bedroom', $bedroom);
				}
				if(!is_null($bathroom))
				{
					$param['bathroom'] = $bathroom;
					$this->setState('search.bathroom', $bathroom);
				}
				if(!is_null($min_price))
				{
					$param['from_price'] = $min_price;
					$this->setState('search.min_price', $min_price);
				}
				if(!is_null($max_price))
				{
					$param['to_price'] = $max_price;
					$this->setState('search.max_price', $max_price);
				}

				// get property
				$strParm = Helper::buildParam($param);
				$url 	= Config::get('global.base_api_url');
				$url 	.=	'property?';
				$url 	.=  $strParm;	
			    $property = RestClient::get($url)->getContent();
				$dataProperty = json_decode($property,true);
				
				// get rooms
				$room = Config::get('global.room');

				// get category types
				$url 	= Config::get('global.base_api_url');
				$url 	.=	'category';
				$url 	.= '?' . $strToken;
				$categoryType = RestClient::get($url)->getContent();
				$datacategoryType = json_decode($categoryType,true);
				
				// get locations
				$country_id = Config::get('global.country_cambodia_id', 1);
				$url 	= Config::get('global.base_api_url');
				$url 	.=	'province?country_id='.$country_id;
				$url 	.= '&' . $strToken;
				$location = RestClient::get($url)->getContent();
				$location = json_decode($location,true);

				$states = $this->states(); 
		
		 } catch (Exception $e) {

		    $message['success'] = false;
		 	$message['message'] = $e->getMessage();
			Session::flash('message', $message);
		}
			
		return View::make('listing.index',array(
				'dataProperty' 	  	 => $dataProperty,
				'room'  => $room,
				'datacategoryType'    => $datacategoryType,
				'location'		  => $location,
				'states'		  => $states
			));
	}

	// load more
	public function postMore()
	{	

 		$page 		 = Input::get('page',1);
 		$pro_rent_sale = Input::get('pro_rent_sale', null);
 		$province_id = Input::get('province_id', null);
 		$pro_category_type = Input::get('pro_category_type', null);
 		$pro_category      = Input::get('pro_category', null);

		$result = array();
		$data = array();

		$param = array(
					'per_page' => $this->limit,
					'sort'     => 'created_at',
					'page'	  => $page,
					'dir'      => 'DESC'
		);
		if(!is_null($pro_rent_sale))
		{
			$param['type'] = $pro_rent_sale;
			$this->setState('search.type',$pro_rent_sale);
		}
		if(!is_null($province_id))
		{
			$param['province_id'] = $province_id;
			$this->setState('search.province_id', $province_id);
		}
		if(!is_null($pro_category_type))
		{
			$param['category'] = $pro_category_type;
			$this->setState('search.pro_category_type', $pro_category_type);
		}
		if(!is_null($pro_category))
		{
			$param['residential_type'] = $pro_category;
			$this->setState('search.pro_category', $pro_category);
		}

		// get property
		$strParm = Helper::buildParam($param);
		$url 	= Config::get('global.base_api_url');
		$url 	.=	'property?';
		$url 	.=  $strParm;	
	    $property = RestClient::get($url)->getContent();
		$data = json_decode($property,true);

		$data = $data['data'];

		$str = '';

		foreach ($data as $key => $value) {

			$properties = $value['property'];

			$str .= '<div class="col-md-4 listing-single-item"><div class="item-inner">';
			$str .= '<div class="image-wrapper">';
			$str .= '<img src="' . $properties['featured_image'] . '">';
			if($properties['is_urgent'])
			{
				$str .= '<a href="#" class="featured"><i class="fa fa-star"></i>featured</a>';
			}
			$str .= '</div>';
			$str .= '<div class="desc-box">';
			$str .= '<h4><a href="'. action('ListingsController@getDetail',$properties['id']) . '">' . Helper::getSubStr($properties['name']) . '</a></h4>';
			$str .= '<ul class="slide-item-features item-features">';
			$str .= '<li>' . '<span class="fa fa-arrows-alt"></span>' . $properties['land_area'];
			if($properties['land_area_unit'])
			{
				$str .= 'sq';
			}
			else
			{
				$str .= 'ha';
			}
			$str .= '</li>';
			$str .= '<li><span class="fa fa-male"></span>' . $properties['bathroom'] . 'bathrooms</li>';
			$str .= '<li><span class="fa fa-inbox"></span>' . $properties['bedroom'] . 'bedrooms</li>';
			$str .= '</ul>';
			$str .= '<div class="buttons-wrapper">';
			$str .= '<a href="#" class="yellow-btn">';
			$str .= Helper::getNumberFormat($properties['price']);
			if($properties['type'])
			{
				$str .= '';
			}
			else
			{
				$str .= ' / pm';
			}
			$str .= '</a>';
			$str .= '<a href="' . action('ListingsController@getDetail',$properties['id']) . '" class="gray-btn">' . '<span class="fa fa-file-text-o"></span>Details' . '</a>';
			$str .= '</div>';
			$str .= '<div class="clearfix"></div>';
			$str .= '</div>';
			$str .= '</div></div>';

		}

		$result['str'] = $str;
		$result['data'] = count($data);

		return $result;
		exit();
	}
}

?>