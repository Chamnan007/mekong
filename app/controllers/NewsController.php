<?php
class NewsController extends BaseController{
		
	public function anyIndex()
 	{
 		$dataPropertyUrgent = array();
 		$page 		 = Input::get('page',1);
		// strToken
		$strToken = Helper::buildTokenURL();

 		try 
		{
			$message = array(
						'success'=>true,
						'message'=>''
					);
			$param['branch_id'] = Config::get('global.branch_id',8);
			$strParm = Helper::buildParam($param);
			$url 	= Config::get('global.base_api_url');
			$url 	.=	'aboutus?';
			$url 	.=  $strParm;
			$companyAbout = RestClient::get($url)->getContent();
			$dataCompanyAbout = json_decode($companyAbout,true);

	        $param['per_page'] = 10;
	        $param['page'] = $page;
		   	$strParm = Helper::buildParam($param);
		   	$url  = Config::get('global.base_api_url');
		   	$url  .= 'events?';
		   	$url  .=  $strParm;
		   	$events = RestClient::get($url)->getContent();
		   	$dataEvents = json_decode($events,true);

			$param['branch_id'] = Config::get('global.branch_id',8);
			$strParm = Helper::buildParam($param);
			$url 	= Config::get('global.base_api_url');
			$url 	.=	'office?';
			$url 	.=  $strParm;
			$companyLogo = RestClient::get($url)->getContent();
			$datacompanyLogo = json_decode($companyLogo,true);

			// urgent property limit 5
  			$paramUrgent['branch_id'] =Config::get('global.branch_id',8);
	        $paramUrgent['per_page'] = 5;
			$paramUrgent['is_urgent'] = Config::get('global.is_urgent');
			$strParm = Helper::buildParam($paramUrgent);
			$url = Config::get('global.base_api_url');
			$url  .= 'property?';
			$url  .=  $strParm;
			$propertyUrgent = RestClient::get($url)->getContent();
			$dataPropertyUrgent = json_decode($propertyUrgent,true);

			// recents property limit 5
  			$paramUrgent['branch_id'] =Config::get('global.branch_id',8);
	        $paramUrgent['per_page'] = 5;
			$strParm = Helper::buildParam($paramUrgent);
			$url = Config::get('global.base_api_url');
			$url  .= 'property?';
			$url  .=  $strParm;
			$propertyRecent = RestClient::get($url)->getContent();
			$dataPropertyRecent = json_decode($propertyRecent,true);

		} catch (Exception $e) {

		    $message['success'] = false;
			$message['message'] = $e->getMessage();
			Session::flash('message', $message);
		}
			
		return View::make('news.index',array(
				'dataCompanyAbout' 	  => $dataCompanyAbout,
				'datacompanyLogo'     => $datacompanyLogo,
				'dataevents' => $dataEvents,
				'dataPropertyUrgent' => $dataPropertyUrgent,
				'dataPropertyRecent' => $dataPropertyRecent,
			));
 	}
 	public function getDetail( $id = 0 ){

		$dataPropertySale = array();
		$dataPropertyRent = array();
		$dataPropertyUrgent = array();
		$datacompanyProfile = array();
		$datacategoryType = array();
		$location  = array();

		// $strToken = Helper::buildTokenURL();
		$strToken = '';

		try 
		{

			if($id) $param['event_id'] = $id;
			// get events
			$param['branch_id'] = Config::get('global.branch_id',8);
			$param['per_page'] = 10;
			$strParm = Helper::buildParam($param);
			$url 	= Config::get('global.base_api_url');
			$url 	.=	'events?';
			$url 	.=  $strParm;
			$events = RestClient::get($url)->getContent();
			$dataEvents = json_decode($events,true);

			// get urgent property
  			$paramUrgent['branch_id'] =Config::get('global.branch_id',8);
	        $paramUrgent['per_page'] = 5;
			$paramUrgent['is_urgent'] = Config::get('global.is_urgent');
			$strParm = Helper::buildParam($paramUrgent);
			$url = Config::get('global.base_api_url');
			$url  .= 'property?';
			$url  .=  $strParm;
			$propertyUrgent = RestClient::get($url)->getContent();
			$dataPropertyUrgent = json_decode($propertyUrgent,true);

			// get recents property
  			$paramUrgent['branch_id'] =Config::get('global.branch_id',8);
	        $paramUrgent['per_page'] = 5;
			$strParm = Helper::buildParam($paramUrgent);
			$url = Config::get('global.base_api_url');
			$url  .= 'property?';
			$url  .=  $strParm;
			$propertyRecent = RestClient::get($url)->getContent();
			$dataPropertyRecent = json_decode($propertyRecent,true);

		} catch (Exception $e) {

		    $message['success'] = false;
			$message['message'] = $e->getMessage();
			Session::flash('message', $message);
		}
		return View::make('news.detail',array(
				'dataevents' 	  		=> $dataEvents['data'][0],
				'dataPropertyUrgent' 	=> $dataPropertyUrgent,
				'dataPropertyRecent' 	=> $dataPropertyRecent
			));
	}
}

?>