<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	protected $shortName = '';
	protected $allStates = array();
	protected function buildShortName()
	{
		if(!$this->shortName){
			$this->shortName  = md5(get_class($this));	
		}
		return $this->shortName;
	}
	public function setState($name,$value)
	{
		$this->shortName = $this->buildShortName();
		$this->allStates = Session::get($this->shortName, array());
		$this->allStates[$name] = $value;
		Session::set($this->shortName, $this->allStates); 

	}

	public function state($name,$default=null)
	{
		$this->shortName = $this->buildShortName();
		$this->allStates = Session::get($this->shortName, array());
		if(isset($this->allStates[$name]))
		{
			return $this->allStates[$name];
		}
		
		return $default;
	}
	public function states()
	{
		$this->shortName = $this->buildShortName();
		$this->allStates = Session::get($this->shortName, array());
		return $this->allStates;
	}

}
