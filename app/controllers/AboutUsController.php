<?php
class AboutUsController extends BaseController{
		
	public function anyIndex(){
		
		$dataAboutUs = array();
		
		try 
		{
			$message = array(
						'success'=>true,
						'message'=>''
					);

			// get about us
			$param['branch_id'] = 8;
			$strParm = Helper::buildParam($param);
			$url 	= Config::get('global.base_api_url');
			$url 	.=	'aboutus?';
			$url 	.=  $strParm;
			$aboutUs = RestClient::get($url)->getContent();
			$dataAboutUs = json_decode($aboutUs,true);

			// get services
			$param['branch_id'] = 8;
			$strParm = Helper::buildParam($param);
			$url 	= Config::get('global.base_api_url');
			$url 	.=	'service?';
			$url 	.=  $strParm;
			$service = RestClient::get($url)->getContent();
			$dataService = json_decode($service,true);
			

			// urgent property limit 5
  			$paramUrgent['branch_id'] =Config::get('global.branch_id',8);
	        $paramUrgent['per_page'] = 5;
			$paramUrgent['is_urgent'] = Config::get('global.is_urgent');
			$strParm = Helper::buildParam($paramUrgent);
			$url = Config::get('global.base_api_url');
			$url  .= 'property?';
			$url  .=  $strParm;
			$propertyUrgent = RestClient::get($url)->getContent();
			$dataPropertyUrgent = json_decode($propertyUrgent,true);

			// recents property limit 5
  			$paramUrgent['branch_id'] =Config::get('global.branch_id',8);
	        $paramUrgent['per_page'] = 5;
			$strParm = Helper::buildParam($paramUrgent);
			$url = Config::get('global.base_api_url');
			$url  .= 'property?';
			$url  .=  $strParm;
			$propertyRecent = RestClient::get($url)->getContent();
			$dataPropertyRecent = json_decode($propertyRecent,true);

		} catch (Exception $e) {

		    $message['success'] = false;
			$message['message'] = $e->getMessage();
			Session::flash('message', $message);
		}

    	return View::make('aboutus.index',array(
				'dataAboutUs'  => $dataAboutUs['data'],
				'dataService'  => $dataService['data'],
				'dataPropertyUrgent' => $dataPropertyUrgent,
				'dataPropertyRecent' => $dataPropertyRecent,
			));
	}
}

?>