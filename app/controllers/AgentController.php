<?php
class AgentController extends BaseController
{
 	
 	public function getDetail($id = 0)
 	{
 		$dataAgentProfile = array();
 		$room = array();
 		$datacategoryType = array();
 		$location    = array();
 		$dataPropertyUrgent = array();

 		$strToken = Helper::buildTokenURL();

 		try 
		{
			$message = array(
						'success'=>true,
						'message'=>''
					);
			if($id)
			{

				// get agent detail
				$url 	= Config::get('global.base_api_url');
				$url 	.=	'agent/detail/'.$id.'?per_page=4';
				$url 	.= '&' . $strToken;
				$agentProfile = RestClient::get($url)->getContent();
				$dataAgentProfile = json_decode($agentProfile,true);

				// get rooms
				$room = Config::get('global.room');

				// get category type
				$url 	= Config::get('global.base_api_url');
				$url 	.=	'category';
				$url .= '?' . $strToken;
				$categoryType = RestClient::get($url)->getContent();
				$datacategoryType = json_decode($categoryType,true);	

				// get locations
				$country_id = Config::get('global.country_cambodia_id', 1);
				$url 	= Config::get('global.base_api_url');
				$url 	.=	'province?country_id='.$country_id;
				$url 	.= '&' . $strToken;
				$location = RestClient::get($url)->getContent();
				$location = json_decode($location,true);
			}
			else 
			{
				return View::make('page.notfound');
			}

		} catch (Exception $e) {

		    $message['success'] = false;
			$message['message'] = $e->getMessage();
			Session::flash('message', $message);
		}
			
		return View::make('agent.detail',array(
				'dataAgentProfile' 	  => $dataAgentProfile,
				'room'  => $room,
				'datacategoryType'    => $datacategoryType,
				'location'		  => $location,
			));
 	}

 	public function anyIndex(){
		
		$dataAboutUs = array();
		
		try 
		{
			$message = array(
						'success'=>true,
						'message'=>''
					);

			// get agent
			$param['branch_id'] = 8;
			$strParm = Helper::buildParam($param);
			$url 	= Config::get('global.base_api_url');
			$url 	.=	'agent?';
			$url 	.=  $strParm;
			$agent = RestClient::get($url)->getContent();
			$dataAgent = json_decode($agent,true);

			// urgent property limit 5
  			$paramUrgent['branch_id'] =Config::get('global.branch_id',8);
	        $paramUrgent['per_page'] = 5;
			$paramUrgent['is_urgent'] = Config::get('global.is_urgent');
			$strParm = Helper::buildParam($paramUrgent);
			$url = Config::get('global.base_api_url');
			$url  .= 'property?';
			$url  .=  $strParm;
			$propertyUrgent = RestClient::get($url)->getContent();
			$dataPropertyUrgent = json_decode($propertyUrgent,true);

			// recents property limit 5
  			$paramUrgent['branch_id'] =Config::get('global.branch_id',8);
	        $paramUrgent['per_page'] = 5;
			$strParm = Helper::buildParam($paramUrgent);
			$url = Config::get('global.base_api_url');
			$url  .= 'property?';
			$url  .=  $strParm;
			$propertyRecent = RestClient::get($url)->getContent();
			$dataPropertyRecent = json_decode($propertyRecent,true);

		} catch (Exception $e) {

		    $message['success'] = false;
			$message['message'] = $e->getMessage();
			Session::flash('message', $message);
		}

    	return View::make('agent.index',array(
				'dataAgent'  => $dataAgent['data'],
				'dataPropertyUrgent' => $dataPropertyUrgent,
				'dataPropertyRecent' => $dataPropertyRecent,
			));
	}

}
?>
