<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	public function anyIndex(){

		$dataProperty   = array();
		$datacompanyProfile = array();
		$datacategoryType = array();
		$location  = array();
		$dataSlideshow = array();
		// strToken
		$strToken = Helper::buildTokenURL();

		try 
		{
			$message = array(
						'success'=>true,
						'message'=>''
					);
			$param = array(
					'branch_id'=>Config::get('global.branch_id',8),
					'per_page' =>6,
					'sort'     => 'created_at',
					'dir'      => 'DESC'
				);
			// get property
			$strParam = Helper::buildParam($param);
			$url 	= Config::get('global.base_api_url');
			$url 	.=	'property?';
			$url 	.=  $strParam;
		    $property = RestClient::get($url)->getContent();
			$dataProperty = json_decode($property,true);
			
			// get slideshow
			$branch_id = Config::get('global.branch_id',8);
			$url 	= Config::get('global.base_api_url');
			$url 	.=	'slideshow?branch_id='.$branch_id;
			$url 	.= '&' . $strToken;
			$slideshow = RestClient::get($url)->getContent();
			$dataSlideshow = json_decode($slideshow,true);

			// get room
			$room = Config::get('global.room');

			// get category type
			$url 	= Config::get('global.base_api_url');
			$url 	.=	'category';
			$url 	.= '?' . $strToken;
			$categoryType = RestClient::get($url)->getContent();
			$datacategoryType = json_decode($categoryType,true);	

			// get locations
			$country_id = Config::get('global.country_cambodia_id', 1);
			$url 	= Config::get('global.base_api_url');
			$url 	.=	'province?country_id='.$country_id;
			$url 	.= '&' . $strToken;
			$location = RestClient::get($url)->getContent();
			$location = json_decode($location,true);

		} catch (Exception $e) {

		    $message['success'] = false;
			$message['message'] = $e->getMessage();
			Session::flash('message', $message);
		}

    	return View::make('home.index',array(
    			'dataProperty' => $dataProperty,
				'datacompanyProfile'  => $datacompanyProfile,
				'room'  => $room,
				'datacategoryType'    => $datacategoryType,
				'location'		  => $location,
				'dataSlideshow'   => $dataSlideshow
			));
	}

	public function postMore()
	{	

		$page = Input::get('page');
		$result = array();
		$data = array();

		$param = array(
					'per_page' => 6,
					'sort'     => 'created_at',
					'dir'      => 'DESC'
				);
		// get property
		$strParam = Helper::buildParam($param);
		$url 	= Config::get('global.base_api_url');
		$url 	.=	'property?page=' . $page . '&';
		$url 	.=  $strParam;
	    $property = RestClient::get($url)->getContent();
		$data = json_decode($property,true);

		$data = $data['data'];

		$str = '';

		foreach ($data as $key => $value) {

			$properties = $value['property'];

			$str .= '<div class="col-md-4 listing-single-item"><div class="item-inner">';
			$str .= '<div class="image-wrapper">';
			$str .= '<img src="' . $properties['featured_image'] . '">';
			if($properties['is_urgent'])
			{
				$str .= '<a href="#" class="featured"><i class="fa fa-star"></i>featured</a>';
			}
			$str .= '</div>';
			$str .= '<div class="desc-box">';
			$str .= '<h4><a href="'. action('ListingsController@getDetail',$properties['id']) . '">' . Helper::getSubStr($properties['name']) . '</a></h4>';
			$str .= '<ul class="slide-item-features item-features">';
			$str .= '<li>' . '<span class="fa fa-arrows-alt"></span>' . $properties['land_area'];
			if($properties['land_area_unit'])
			{
				$str .= 'sq';
			}
			else
			{
				$str .= 'ha';
			}
			$str .= '</li>';
			$str .= '<li><span class="fa fa-male"></span>' . $properties['bathroom'] . 'bathrooms</li>';
			$str .= '<li><span class="fa fa-inbox"></span>' . $properties['bedroom'] . 'bedrooms</li>';
			$str .= '</ul>';
			$str .= '<div class="buttons-wrapper">';
			$str .= '<a href="#" class="yellow-btn">';
			$str .= Helper::getNumberFormat($properties['price']);
			if($properties['type'])
			{
				$str .= '';
			}
			else
			{
				$str .= ' / pm';
			}
			$str .= '</a>';
			$str .= '<a href="' . action('ListingsController@getDetail',$properties['id']) . '" class="gray-btn">' . '<span class="fa fa-file-text-o"></span>Details' . '</a>';
			$str .= '</div>';
			$str .= '<div class="clearfix"></div>';
			$str .= '</div>';
			$str .= '</div></div>';

		}

		$result['str'] = $str;
		$result['data'] = count($data);

		return $result;
		exit();
	}

}
