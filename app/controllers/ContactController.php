<?php
class ContactController extends BaseController{
		
	public function anyIndex(){
		
		$datacompanyProfile = array();
		$datacategoryType = array();
		$location  = array();

		$strToken = Helper::buildTokenURL();

		try 
		{
			$message = array(
						'success'=>true,
						'message'=>''
					);

			// get company profile
			$param['branch_id'] = 8;
			$strParm = Helper::buildParam($param);
			$url 	= Config::get('global.base_api_url');
			$url 	.=	'profile?';
			$url 	.=  $strParm;
			$companyProfile = RestClient::get($url)->getContent();
			$datacompanyProfile = json_decode($companyProfile,true);

			// get logo
			$param['branch_id'] = 8;
			$strParm = Helper::buildParam($param);
			$url 	= Config::get('global.base_api_url');
			$url 	.=	'office?';
			$url 	.=  $strParm;
			$companyLogo = RestClient::get($url)->getContent();
			$datacompanyLogo = json_decode($companyLogo,true);

			// get rooms
			$room = Config::get('global.room');

			// get category types
			$url 	= Config::get('global.base_api_url');
			$url 	.=	'category';
			$url 	.= '?' . $strToken;
			$categoryType = RestClient::get($url)->getContent();
			$datacategoryType = json_decode($categoryType,true);	
			
			// get locations
			$country_id = Config::get('global.country_cambodia_id', 1);
			$url 	= Config::get('global.base_api_url');
			$url 	.=	'province?country_id='.$country_id;
			$url 	.= '&' . $strToken;
			$location = RestClient::get($url)->getContent();
			$location = json_decode($location,true);

		} catch (Exception $e) {

		    $message['success'] = false;
			$message['message'] = $e->getMessage();
			Session::flash('message', $message);
		}

    	return View::make('contact.index',array(
				'datacompanyLogo'		  => $datacompanyLogo,
				'datacompanyProfile'  => $datacompanyProfile,
				'room'  => $room,
				'datacategoryType'    => $datacategoryType,
				'location'		  => $location,
			));
	}
	public function postSentMail()
	{
		
		$data = array();
		
		$name    = Input::get('name');
		$website    = Input::get('website');
		$email   = Input::get('email');
		$content = Input::get('message');
			
		$param['branch_id'] = 8;
		$strParm = Helper::buildParam($param);

		$url 	= Config::get('global.base_api_url');
		$url 	.=	'profile?';
		$url 	.=  $strParm;
		$companyProfile = RestClient::get($url)->getContent();
		$datacompanyProfile = json_decode($companyProfile,true);
		
		$contactName = '';
		$contactEmail = '';
		if(count($datacompanyProfile))
		{
			$contactEmail = $datacompanyProfile['data']['email'];
			$contactName  = $datacompanyProfile['data']['company_name'];
		}
	    $data = array(
	    	'name' =>$name,
	    	'email' =>$email,
	    	'website' => $website,
	    	'content' => $content,
	    	'contactName' => $contactName
	    );
	    $flash = array(
				'success'=>false,
				'message'=>trans('layout.sent_mail_fail')
		);
		try{
			Mail::send('elements.mail_branch', $data, function($message) use ($email,$name,$contactEmail,$contactName)
	    	{   
		       /* $message->from($email, $name);*/
		        $message->to($contactEmail, $contactName)->subject('User Contact');
	    	});
	    	$text_message = trans('layout.sent_mail_success');
			$flash['message'] = $text_message;
			$flash['success'] = true;
			
		} catch (Exception $e) 
		{
			$flash['message'] = $e->getMessage();
		}
		
		return $flash;
		exit();	
	}
}

?>