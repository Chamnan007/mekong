<?php 

/**
 * @author Engleang SAM
 * @since  Aug 15 2014
 * Helper class for use in view 
 * Generate Button 
 * Flash Message etc
 */
 class Helper 
 {
 	public static $currentAction = null;
 	public static $moreAction = array(); 
 	private static $defaultController = 'DefaultController';
 	public static function setDefaultController($controllerName)
 	{
 			self::$defaultController = $controllerName;
 	}
 	public static function  getDefaultController()
 	{
 		return self::$defaultController;
 	}

 	public static function getMoreActionUrl()
 	{
 		return self::$moreAction;
 	}
 	
 	public static function getEditButton($url)
 	{
 		return '<a href="'.$url.'" 
 					rel="tooltip"
 					class="edit-button"
 					data-toggle="tooltip" data-original-title="Click To Edit"
 				>
 					<i class="fa fa-edit"></i>
 				</a>&nbsp;';

 	}
 	public static function getDeleteButton($url,$id=0)
 	{
 		return '<a
 					class="trash-button"
 					data-id="'.$id.'" 
 					href="'.$url.'" 
 					rel="tooltip"
 					data-toggle="tooltip" data-original-title="Click To Delete"
 				>
 					<i class="fa  fa-trash-o"></i>
 				</a>&nbsp;';
 	}
 	public static function getViewButton($url)
 	{
 		return '<a href="'.$url.'" 
 					rel="tooltip"
 					class="view-button"
 					data-toggle="tooltip" data-original-title="Click To View"
 				>
 					<i class="fa  fa-eye"></i>
 				</a>&nbsp;';
 	}
 	public static function getCheckAllButton()
 	{
 		return '<input type="checkbox" class="btn-check-all simple" 
 					rel="tooltip"
 					id="btn-check-all"
 					data-toggle="tooltip" data-original-title="Check All" >';
 	}
 	public static function getCheckButton($id)
 	{
 		return '<input type="checkbox" name="ckb[]" class="ckb simple" value="'.$id.'" id="cb'.$id.'" >';
 	}
 	public static function getFlashMessage()
 	{
 		$class = 'alert';
 		if(Session::has('message'))
 		{
 			$message = Session::get('message');
 			$prefix = '<strong>Warning!</strong>';
 			if($message['success'])
 			{
 				$class.=' alert-success';
 				$prefix = '<strong>Info!</strong>';
 			}
 			else{
 				$class.=' alert-warning';
 			}
 			return 
	 			'<div class="'.$class.'">
	        		<a href="#" class="close" data-dismiss="alert">&times;</a>
	        	'.$prefix.' '.$message['message'].'
	    		</div>';
 		}
 		return '';
 	}
 	public static function getMenuActiveClass($param)
 	{
 		if(self::$currentAction ==null)
 		{
 			self::$currentAction  = Route::currentRouteAction();
 		}
 		if(is_array($param))
 		{
 			if(in_array(self::$currentAction, $param))
 			{
 				return 'active';
 			}
 		}
 		elseif(self::$currentAction == $param)
 		{
 			return 'active';
 		}

 		return '';
 	}

 	public static function getStateButton($state,$url,$permission=true)
 	{
 		$url =  $url;
 		if(!$permission)$url = '#';
 		$button = array(
 				0=>'<a 
 						data-toggle="tooltip" 
 						data-original-title="Click To Activate"
 					 	href="'.$url.'" class="btn btn-xs btn-warning deactive_button">
 					 		<i class="fa fa-times"></i> inactive
 					 	</a>',
 				1=>'<a 
 					data-toggle="tooltip" 
 					data-original-title="Click To Deactivate"
 						 href="'.$url.'" class="btn btn-xs btn-primary active_button">
 					<i class="fa fa-check"></i>active</a>',
 			);
 		if(isset($button[$state]))
 		{
 			return $button[ $state];
 		}
 		return '';
 	}
 	public static function getDefault($url,$is_default=false)
 	{
 		$obj = (int)$is_default;
 		$button = array(
			0=>'<a 
					data-toggle="tooltip" 
					data-original-title="Click To Set As Default"
				 	href="'.$url.'" class="btn btn-xs btn-warning">
				 		<i class="fa   fa-ban"></i> 
				 	</a>',
			1=>'<a 
				
					 href="'.$url.'" class="btn btn-xs btn-primary">
				<i class="fa fa-check"></i></a>',
 			);
 		return $button[$obj];
 	}
 	public static function getRecordNumber($selected=10)
	{
		$selects = array(1,10,30,50,100,300,500,1000);
		$st 	= '';
		foreach ($selects as $key => $value) 
		{
			$stSelect = '';
			if($value==$selected)$stSelect = ' selected = "selected"';
			$st.='<option '.$stSelect.'>'.$value.'</option>';
		}
		return '<select name="per_page" id="record_number" class="col-sm-4">
				'.$st.'
				</select>';	
	}
	public static function buildParam(array $param)
	{
		$str = "";
		$seperator = "";
		foreach ($param as $key => $value) 
		{
			$str  = $str.$seperator.$key.'='.$value;
			$seperator = "&";
		}
		// token
		$str .= "&app_id=" . APP_ID . "&token=" . TOKEN;
		// end 
		return $str;
	}
	public static function buildTokenURL()
	{
		$str = "app_id=" . APP_ID . "&token=" . TOKEN;
		return $str;
	}
	// format number
    public static function getNumberFormat($number){
        return number_format($number);
    }
    // format phone number
    public static function getPhoneNumberFormat($phone_number = '')
    {
		if(  preg_match( '/^\d(\d{2})(\d{3})(\d{3,4})$/', $phone_number,  $matches ) )
		{
		    $result = $matches[1] . '-' .$matches[2] . '-' . $matches[3];
		    return $result;
		}		
		else
		{
			return $phone_number;
		}

    }
    public static function getSubStr($name)
    {
        $name = strip_tags($name);
        if(strlen($name) > 40)
        {
            return substr($name, 0 ,40)."...";
        }
        return $name;
    }
    public static function getSubDes($name)
    {
        $name = strip_tags($name);
        if(strlen($name) > 150)
        {
            return substr($name, 0 ,150)."...";
        }
        return $name;
    }
    public static function getSubCap($name)
    {
        $name = strip_tags($name);
        if(strlen($name) > 15)
        {
            return substr($name, 0 ,15)."...";
        }
        return $name;
    }
    public static function getSubDes300($name)
    {
        $name = strip_tags($name);
        if(strlen($name) > 300)
        {
            return substr($name, 0 ,300)."...";
        }
        return $name;
    }
    public static function getSubDes500($name)
    {
        $name = strip_tags($name);
        if(strlen($name) > 500)
        {
            return substr($name, 0 ,500)."...";
        }
        return $name;
    }
 	// generate token
 	public static  function generateToken($appId = "")
 	{
		$secretKey = array(	"Phnom Penh", 
							"Small Night", 
							"Red Haired", 
							"Good Cop", 
							"Smash You",
				   			"Technologies Nowadays",  
				   			"ABI", 
				   			"Console Log", 
				   			"Sub String", 
				   			"Old Computer");

		$index 		= rand(0,9);
		$second 	=  date('s');
		$datetime 	= date('Y-m-d H:i:s');
		
		$hash = sha1($appId . ":" . $secretKey[$index] . " " . $datetime);
		
		for ($i = 0; $i < $second; $i++) 
		{
		 	$hash = sha1($hash);
		}
		
		return urlencode( $hash . '@' . $index . $datetime);

 	}
 	// validate token
 	public static  function validateToken($appId = "", $token = "")
 	{
		$secretKey = array(	"Phnom Penh", 
							"Small Night", 
							"Red Haired", 
							"Good Cop", 
							"Smash You",
				   			"Technologies Nowadays",  
				   			"ABI", 
				   			"Console Log", 
				   			"Sub String", 
				   			"Old Computer");
				   
		$data = explode("@", $token);

		if(count($data)<2) return false;
		$index = substr($data[1], 0, 1);
		$datetime = substr($data[1], 1);
		$dateitem = explode(":", $datetime);
		$count = $dateitem[2];

		$submit_time = strtotime($datetime);
		$current_time = time();
		
		$diff = round(($current_time - $submit_time)/(60*60));
		
		
		if ($diff >= 24) {
			return false;
		}
		
		$hash = sha1($appId . ":" . $secretKey[$index] . " " . $datetime);
		
		for ($i = 0; $i < $count; $i++) {
			$hash = sha1($hash);
		}
		
		
		if ($data[0] == $hash) {
			return true;
		}
				
		
		return false;
 	}
} 
?>