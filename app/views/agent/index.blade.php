@extends('layout.default')

@section('slider')
<div class="page-title-section">
		<div class="container">
			<div class="pull-left page-title">
				<a href="#">
				<h2>Agent listing</h2>
				</a>
			</div>
			<div class="pull-right breadcrumb">
				<a href="{{action('HomeController@anyIndex')}}">home</a><span class="fa fa-arrow-circle-right sep"></span><a>about us</a>
			</div>
		</div>
</div>
@stop

@section('search_section')

@stop

<!-- content-Section -->
@section('content')

<?php 

$dataUrgent = $dataPropertyUrgent['data'];
$dataRecent = $dataPropertyRecent['data'];

?>

<div class="content-section">
	<div class="container">
		<div class="col-md-8 blog-content">
			@foreach( $dataAgent as $key => $value )
				<div class="single-agent">
					<div class="image-box">
						<img src="{{$value['featured_image']}}">
						<ul class="social-icons">
							<li><a href="#" class="fa fa-google-plus"></a></li>
							<li><a href="#" class="fa fa-facebook"></a></li>
						</ul>
					</div>
					<div class="desc-box">
						<h4>{{$value['first_name'] . " " . $value['last_name']}}</h4>
						<p class="person-number">
							<i class="fa fa-phone"></i> {{$value['phone1']}}
						</p>
						<p class="person-email">
							<i class="fa fa-envelope"></i> {{$value['email']}}
						</p>
						<a href="{{action('AgentController@getDetail',$value['id'])}}" class="gray-btn">More Detail</a>
					</div>
				</div>
			@endforeach
		</div>
		<div class="col-md-4 blog-sidebar">
			<div class="sidebar-widget tabbed-content">
				<ul class="nav nav-tabs" id="myTab">
					<li class="active"><a href="#popular" data-toggle="tab">featured</a></li>
					<li class=""><a href="#recent" data-toggle="tab">recent</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade active in" id="popular">
						<ul class="tab-content-wrapper">
							@foreach( $dataUrgent as $uKey => $uValue )
								<li class="tab-content-item">
									<div class="pull-left thumb">
										<img src="{{@$uValue['property']['featured_image']}}">
									</div>
									<h5>
										<a href="#">{{$uValue['property']['name'] . " - $ " . Helper::getNumberFormat($uValue['property']['price'])}}</a>
									</h5>
								</li>
							@endforeach
						</ul>
					</div>
					<div class="tab-pane fade" id="recent">
						<ul class="tab-content-wrapper">
							@foreach( $dataRecent as $rKey => $rValue )
								<li class="tab-content-item">
									<div class="pull-left thumb">
										<img src="{{@$rValue['property']['featured_image']}}">
									</div>
									<h5>
										<a href="#">{{$rValue['property']['name'] . " - $ " . Helper::getNumberFormat($rValue['property']['price'])}}</a>
									</h5>
								</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
	.single-agent
	{
		width: 50%;
	}
	.single-agent .image-box 
	{
		width: 35% !important;
	}
	.desc-box
	{
		width: 65% !important;
		height: 180px !important;
		padding: 8px !important;
	}
	.single-agent .desc-box p
	{
		margin: 5px 0;
	}
	.single-agent .image-box
	{
		height: 180px !important;
	}
	.gray-btn:hover
	{
		background-color: #fff !important;
		color: #000 !important;
	}
	.single-agent .image-box .social-icons
	{
		background-color: rgb(251, 206, 17) !important;
	}
</style>

@stop