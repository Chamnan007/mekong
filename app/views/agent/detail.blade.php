@extends('layout.default')
@section('title','Agent Profile')
@section('header','Property Listing in Cambodia')
@section('slider')
<div class="page-title-section">
		<div class="container">
			<div class="pull-left page-title">
				<a href="#">
				<h2>{{trans('layout.agent_detail')}}</h2>
				</a>
			</div>
			<div class="pull-right breadcrumb">
				<a href="{{action('HomeController@anyIndex')}}">{{trans('layout.home')}}</a><span class="fa fa-arrow-circle-right sep">
				</span><a>{{trans('layout.profile')}}</a>
			</div>
		</div>
</div>
@stop

@section('search_section')
<?php 
    $agentProfile = array();
    if(isset($dataAgentProfile['data']))
    {
        $agentProfile = $dataAgentProfile['data'];
    }
    $dataCategory = array();
    $locationArray = array();
    if(isset($datacategoryType['data']))
    {
    	$dataCategory = $datacategoryType['data'];
    }
    if(isset($location['data']))
    {
    	$locationArray = $location['data'];
    }
?> 
<div class="search-section">
	<div class="container">
		{{Form::open(
                    array(
                        'class'=>'form-horizontal',
                        'action'=>array("ListingsController@anyIndex"),
                        'method'=>'post',
                        'id'=>'adminForm'
                    )
            )}}
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.rent_sale')}}
				</p>
				<select class='elselect' name="pro_rent_sale">
					<option value="">{{trans('layout.any')}}</option>
					<option value="0">{{trans('layout.rent')}}
					</option>
					<option value="1">{{trans('layout.sale')}}
					</option>
				</select>
			</div>
			<div class="select-wrapper select-medium"><!--Vila/Flat/House-->
				<p>
					{{trans('layout.property_type')}}
				</p>
				<select class='elselect' name="pro_category_type">
						<option value="">{{trans('layout.any')}}</option>
                        @foreach($dataCategory as $key => $value)
                            <option value="{{$value['id']}}">
                            {{$value['name']}}
                            </option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-medium">
				<p>
					{{trans('layout.property_category')}}
				</p>
				<select class='elselect' name="pro_category"><!--other category-->
					<option value="">{{trans('layout.any')}}</option>
                    <option value="1">Commercial</option>
                    <option value="0">Residential</option>
				</select>
			</div>
			<div class="select-wrapper select-medium">
				<p>
					{{trans('layout.location')}}
				</p>
				<select class='elselect' name="province_id">
					<option value="">{{trans('layout.any')}}</option>
                        @foreach($locationArray as $key => $value)
                            <option value="{{$value['id']}}">
                            	{{$value['name']}}
                        	</option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.bedroom')}}
				</p>
				<select class='elselect' name="bedroom">
					<option value = ''>{{trans('layout.any')}}</option>
                        @foreach($room as $key => $value)
                        <option value='{{$key}}'>{{$value}}
                        </option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.bathroom')}}
				</p>
				<select class='elselect' name="bathroom">
					<option value = ''>{{trans('layout.any')}}</option>
                        @foreach($room as $key => $value)
                        <option value='{{$key}}'>
                            {{$value}}
                        </option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-small ">
				<p>
					{{trans('layout.min_price')}}
				</p>
				<input type="number" min="0" step="1" name="min_price" 
						placeholder="{{trans('layout.min_price')}}" value=""> 
			</div>
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.max_price')}}
				</p>
				<input type="number" min="0" step="1" name="max_price" 
					   value="" 
					   placeholder="{{trans('layout.min_price')}}"> 
			</div>
			<div class="select-wrapper select-medium">
				<button  class="yellow-btn" id="btn_save"> 
	                    {{trans('layout.search')}}
	            </button>
        	</div>
		</form>
	</div>
</div>
@stop
@section('content')
<div class="content-section">
	<div class="container">
		<div class="row">
			<div class="col-md-4 blog-sidebar">
				<div class="sidebar-widget author-profile">
					<h4 class="widget-title">{{trans('layout.profile')}}</h4>
					<div class="image-box">
						<img src="{{$agentProfile['featured_image']}}" alt="agent">
						<ul class="social-icons">
							<li><a href="#" class="fa fa-google-plus"></a></li>
							<li><a href="#" class="fa fa-twitter"></a></li>
							<li><a href="#" class="fa fa-pinterest"></a></li>
							<li><a href="#" class="fa fa-dribbble"></a></li>
							<li><a href="#" class="fa fa-linkedin"></a></li>
							<li><a href="#" class="fa fa-facebook"></a></li>
						</ul>
					</div>
					<div class="desc-box">
						<h4>{{$agentProfile['first_name'].' '.$agentProfile['last_name']}}</h4>
						<p>{{$agentProfile['position']}}</p>
						<p class="person-number">
							<i class="fa fa-phone"></i>(+855) {{Helper::getPhoneNumberFormat($agentProfile['phone1'])}}
						</p>
						@if(!is_null($agentProfile['phone2']))
						<p class="person-number">
							<i class="fa fa-phone"></i>(+855) {{Helper::getPhoneNumberFormat($agentProfile['phone2'])}}
						</p>
						@endif
						<p class="person-email">
							<i class="fa fa-envelope"></i>{{$agentProfile['email']}}
						</p>
						<p class="person-email">
							<i class="fa fa-file-text-o"></i>{{$agentProfile['description']}}
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-8 page-content">
				
				<div class="row listings-items-wrapper">
				<?php 
			        $agentProperty = array();
			        if(isset($dataAgentProfile['properties']))
			        {
			            $agentProperty = $dataAgentProfile['properties'];
			        }
			        $count = 0;
			    ?> 
			    @foreach($agentProperty as $key => $value)
			    <?php $count++; ?>
					<div class="col-md-6 listing-single-item">
						<div class="item-inner">
							<div class="image-wrapper">
								<img src="{{@$value['featured_image']}}" alt="gallery">
								@if(@$value['is_urgent'])
								<a href="#" class='featured'><i class='fa fa-star'></i>featured</a>
								@endif
							</div>
							<div class="desc-box">
								<h4><a href="{{action('ListingsController@getDetail',$value['id'])}}">{{Helper::getSubStr($value['name'])}}</a></h4>
								<ul class="slide-item-features item-features">
									<li><span class="fa fa-arrows-alt"></span>
										{{$value['land_area']}}
										@if($value['land_area_unit'])
		                                sq
		                                @else
		                                ha
		                                @endif
									</li>
									<li><span class="fa fa-male"></span>{{$value['bathroom']}} bathrooms</li>
									<li><span class="fa fa-inbox"></span>{{$value['bedroom']}} bedrooms</li>
								</ul>
								<div class="buttons-wrapper">
									<a href="#" class="yellow-btn">
										&dollar;{{Helper::getNumberFormat($value['price'])}}
		                                @if($value['type'])
		                                    {{''}}
		                                @else
		                                    / pm
		                                @endif
		                            </a>
									<a href="{{action('ListingsController@getDetail',$value['id'])}}" class="gray-btn">
										<span class="fa fa-file-text-o"></span>Details</a>
								</div>
								<div class="clearfix">
								</div>
							</div>
						</div>
					</div>
				@endforeach

				@if($count == 0)
					<span class="no_record">{{trans('layout.no_record_found')}}</span>
				@endif
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('script')
@parent
<script type="text/javascript">
	
</script>
@stop




