@extends('layout.default')

@section('slider')
<div class="page-title-section">
		<div class="container">
			<div class="pull-left page-title">
				<a href="#">
				<h2>property listing</h2>
				</a>
			</div>
			<div class="pull-right breadcrumb">
				<a href="{{action('HomeController@anyIndex')}}">home</a><span class="fa fa-arrow-circle-right sep"></span><a>about us</a>
			</div>
		</div>
</div>
@stop

@section('search_section')

@stop

<!-- content-Section -->
@section('content')

<?php 

$data = array();
$dataUrgent = array();
$dataRecent = array();

$data = $dataevents['data'];
$dataUrgent = $dataPropertyUrgent['data'];
$dataRecent = $dataPropertyRecent['data'];

?>

<div class="content-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 blog-content">
				@foreach($data as $key => $value)
					<div class="blog-post">
						<div class="post-content">
							<h2><a href="#">{{$value['subject']}}</a></h2>
							<p>
								<i class="fa fa-calendar-o"></i>
								<span>{{date("d-F-Y", strtotime($value['start_date']))}}</span>
							</p>
							<div class="featured-image col-md-6 no-padding">
								<img src="{{$value['featured_image']}}" alt="blog">
								<a href="{{action('NewsController@getDetail',$value['id'])}}" class="readmore-btn">more</a>
							</div>
							<div class="col-md-6">
								{{Helper::getSubDes500($value['description'])}}
							</div>
						</div>
					</div>
					<div class="post-divider"></div>
				@endforeach
			</div>
			<div class="col-md-4 blog-sidebar">
				<div class="sidebar-widget tabbed-content">
					<ul class="nav nav-tabs" id="myTab">
						<li class="active"><a href="#popular" data-toggle="tab">featured</a></li>
						<li class=""><a href="#recent" data-toggle="tab">recent</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade active in" id="popular">
							<ul class="tab-content-wrapper">
								@foreach( $dataUrgent as $uKey => $uValue )
									<li class="tab-content-item">
										<div class="pull-left thumb">
											<img src="{{@$uValue['property']['featured_image']}}">
										</div>
										<h5>
											<a href="#">{{$uValue['property']['name'] . " - $ " . Helper::getNumberFormat($uValue['property']['price'])}}</a>
										</h5>
									</li>
								@endforeach
							</ul>
						</div>
						<div class="tab-pane fade" id="recent">
							<ul class="tab-content-wrapper">
								@foreach( $dataRecent as $rKey => $rValue )
									<li class="tab-content-item">
										<div class="pull-left thumb">
											<img src="{{@$rValue['property']['featured_image']}}">
										</div>
										<h5>
											<a href="#">{{$rValue['property']['name'] . " - $ " . Helper::getNumberFormat($rValue['property']['price'])}}</a>
										</h5>
									</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="pagination-wrapper">
			<ul class="pagination">
	            <?php $active = ' '; ?>
	            @if(isset($dataevents['last_page']))
					@if($dataevents['last_page'] > 1)
						@for ($i=1; $i <= $dataevents['last_page'] ; $i++) 
							@if($i == $dataevents['current_page'] )
								<?php $active = ' active'; ?>
								<li class="{{$active}}"><a href="#">{{$i}}</a></li>
							@else
								<li class=""><a href="?page={{$i}}">{{$i}}</a></li>
							@endif 
						@endfor
					@endif
	            @endif
			</ul>
		</div>
	</div>
</div>

@stop