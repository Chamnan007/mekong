@extends('layout.default')
@section('title','Property Listing')
@section('header','Property Listing in Cambodia')
@section('slider')
<div class="page-title-section">
		<div class="container">
			<div class="pull-left page-title">
				<a href="#">
				<h2>{{trans('layout.property_listing')}}</h2>
				</a>
			</div>
			<div class="pull-right breadcrumb">
				<a href="{{action('HomeController@anyIndex')}}">{{trans('layout.home')}}</a><span class="fa fa-arrow-circle-right sep">
				</span><a>{{trans('layout.listing')}}</a>
			</div>
		</div>
</div>
@stop

@section('search_section')
<?php 
	$dataCategory = array();
	$locationArray = array();
	if(isset($datacategoryType['data']))
    {
    	$dataCategory = $datacategoryType['data'];
    }
    if(isset($location['data']))
    {
    	$locationArray = $location['data'];
    }
?>
<div class="search-section">
	<div class="container">
		{{Form::open(
                    array(
                        'class'=>'form-horizontal',
                        'action'=>array("ListingsController@anyIndex"),
                        'method'=>'post',
                        'id'=>'adminForm'
                    )
            )}}
			<div class="select-wrapper select-big">
				<p>
					{{trans('layout.rent_sale')}}
				</p>
				<select class='elselect' name="pro_rent_sale" id="pro_rent_sale">
					<option value="">{{trans('layout.any')}}</option>
					<option value="0" <?php if(@$states['search.type'] =='0') echo 'selected' ?>>{{trans('layout.rent')}}
					</option>
					<option value="1" <?php if(@$states['search.type'] =='1') echo 'selected' ?>>{{trans('layout.sale')}}
					</option>
				</select>
			</div>
			<div class="select-wrapper select-big"><!--Vila/Flat/House-->
				<p>
					{{trans('layout.property_type')}}
				</p>
				<select class='elselect' name="pro_category_type" id="pro_category_type">
						<option value="">{{trans('layout.any')}}</option>
                        @foreach($dataCategory as $key => $value)
                            <option value="{{$value['id']}}" <?php 
                            if(@$states['search.pro_category_type'] == $value['id']) echo 'selected' ?>>
                            {{$value['name']}}
                            </option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-big">
				<p>
					{{trans('layout.property_category')}}
				</p>
				<select class='elselect' name="pro_category" id="pro_category"><!--other category-->
					<option value="">{{trans('layout.any')}}</option>
                    <option value="1" 
                    	<?php 
                            if(@$states['search.pro_category'] =='1') echo 'selected' ?>
                        >Commercial</option>
                    <option value="0" <?php 
                            if(@$states['search.pro_category'] =='0') echo 'selected' ?>>Residential</option>
				</select>
			</div>
			<div class="select-wrapper select-big">
				<p>
					{{trans('layout.location')}}
				</p>
				<select class='elselect' name="province_id" id="province_id">
					<option value="">{{trans('layout.any')}}</option>
                        @foreach($locationArray as $key => $value)
                            <option value="{{$value['id']}}" <?php 
                            if(@$states['search.province_id'] == $value['id']) echo 'selected' ?>>
                            	{{$value['name']}}
                        	</option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-medium">
				<button  class="yellow-btn" id="btn_save"> 
	                    {{trans('layout.search')}}
	            </button>
        	</div>
			<!-- 
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.bedroom')}}
				</p>
				<select class='elselect' name="bedroom">
					<option value = ''>{{trans('layout.any')}}</option>
                        @foreach($room as $key => $value)
                        <option value='{{$key}}' <?php 
                            if(@$states['search.bedroom'] === (string)$key) echo 'selected' ?>>{{$value}}
                        </option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.bathroom')}}
				</p>
				<select class='elselect' name="bathroom">
					<option value = ''>{{trans('layout.any')}}</option>
                        @foreach($room as $key => $value)
                        <option value='{{$key}}' <?php 
                            if(@$states['search.bathroom'] === (string)$key) echo 'selected' ?>>
                            {{$value}}
                        </option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-small ">
				<p>
					{{trans('layout.min_price')}}
				</p>
				<input type="number" min="0" step="1" name="min_price" 
						placeholder="{{trans('layout.min_price')}}" value="{{@$states['search.max_price']}}"> 
			</div>
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.max_price')}}
				</p>
				<input type="number" min="0" step="1" name="max_price" 
					   value="{{@$states['search.max_price']}}" 
					   placeholder="{{trans('layout.min_price')}}"> 
			</div>
			-->
		</form>
	</div>
</div>

@stop

@section('content')
<div class="content-section">
	<div class="container">
		<div class="row listings-items-wrapper" id="property-container">
			<?php 
            	$properties = array();
                if(isset($dataProperty['data']))
                {
                    $properties = $dataProperty['data'];
                }
        		$count = 0; 
        		$total_pro = count($properties);
        	?>
			@foreach ($properties as $i=>$pro)                      
            <?php $count++; 
                $property = array();
                if(isset($pro['property']))
                {
                    $property = $pro['property'];
                }
            ?> 
			<div class="col-md-4 listing-single-item">
				<div class="item-inner">
					<div class="image-wrapper">
						<img src="{{$property['featured_image']}}" alt="gallery">
						@if($property['is_urgent'])
						<a href="#" class='featured'><i class='fa fa-star'></i>featured</a>
						@endif
					</div>
					<div class="desc-box">
						<h4><a href="{{action('ListingsController@getDetail',$property['id'])}}">{{Helper::getSubStr($property['name'])}}</a></h4>
						<ul class="slide-item-features item-features">
							<li><span class="fa fa-arrows-alt"></span>
								{{$property['land_area']}}
								@if($property['land_area_unit'])
                                sq
                                @else
                                ha
                                @endif
							</li>
							<li><span class="fa fa-male"></span>{{$property['bathroom']}} bathrooms</li>
							<li><span class="fa fa-inbox"></span>{{$property['bedroom']}} bedrooms</li>
						</ul>
						<div class="buttons-wrapper">
							<a href="#" class="yellow-btn">
								&dollar;{{Helper::getNumberFormat($property['price'])}}
                                @if($property['type'])
                                    {{''}}
                                @else
                                    / pm
                                @endif
                            </a>
							<a href="{{action('ListingsController@getDetail',$property['id'])}}" class="gray-btn">
								<span class="fa fa-file-text-o"></span>Details</a>
						</div>
						<div class="clearfix">
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
		@if($count == 0)
			<span class="no_record">{{trans('layout.no_record_found')}}</span>
		@elseif($count > 6)
		<div class="col-sm-12 load-more-container">
			<a href="#" class="gray-btn load-more" id="load-more">Load More</a>
		</div>
		@endif
		<!-- 
		<div class="pagination-wrapper">
			<ul class="pagination">
				<?php $active = ' '; ?>
				@if(isset($dataProperty['last_page']))
					@if($dataProperty['last_page'] > 1)
					@for ($i=1; $i <= $dataProperty['last_page'] ; $i++) 
						@if($i == $dataProperty['current_page'] )
							<?php $active = ' active'; ?>
							<li class="{{$active}}"><a href="#">{{$i}}</a></li>
						@else
							<li class=""><a href="?page={{$i}}">{{$i}}</a></li>
						@endif 
					 	
					@endfor
					@endif
				@endif
			</ul>
		</div> -->
	</div>
</div>
<style type="text/css">
.load-more-container
{
	margin-top: 40px;
	text-align: center;
}
</style>
@stop
@section('script')
@parent
<script type="text/javascript">
  	$('document').ready(function(){
  		var page = 1;

  		$('#load-more').click(function(event){
  			event.preventDefault();
	  		// get search params
	  		var get_rent = $('#pro_rent_sale').val(),
	  			get_type = $('#pro_category_type').val(),
	  			get_cat = $('#pro_category').val(),
	  			get_loc = $('#province_id').val();

  			page++;
  			getMore(page, get_rent, get_type, get_cat, get_loc);

  		});
  	});
  	function getMore(page, rent_tye, type, category, location)
  	{
  		$.ajax({
  			url : '{{action("ListingsController@postMore")}}',
  			type : 'POST',
  			data : { 
  					page : page,
  					pro_rent_sale : rent_tye,
  					pro_category_type: type,
  					pro_category: category,
  					province_id: location
  					},
  			beforeSend:function()
  			{
  			},
  			success:function(data)
  			{
  				console.log(data);
  				var count = data.data,
  					str = data.str;
  				$('#property-container').append(str);
  				// hide load more button if no more property
  				if(count < 9)
  				{
  					$('#load-more').fadeOut();
  				}
  			}
  		});
  	}
</script>
@stop