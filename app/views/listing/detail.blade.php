@extends('layout.default')
@section('title','Property Listing')
@section('header','Property Listing in Cambodia')
@section('slider')
<div class="page-title-section">
		<div class="container">
			<div class="pull-left page-title">
				<a href="#">
				<h2>{{trans('layout.property_detail')}}</h2>
				</a>
			</div>
			<div class="pull-right breadcrumb">
				<a href="{{action('HomeController@anyIndex')}}">{{trans('layout.home')}}</a><span class="fa fa-arrow-circle-right sep">
				</span><a>{{trans('layout.detail')}}</a>
			</div>
		</div>
</div>
@stop

@section('search_section')
<?php 
	$dataCategory = array();
	$locationArray = array();
	if(isset($datacategoryType['data']))
    {
    	$dataCategory = $datacategoryType['data'];
    }
    if(isset($location['data']))
    {
    	$locationArray = $location['data'];
    }
?>
<div class="search-section">
	<div class="container">
		{{Form::open(
                    array(
                        'class'=>'form-horizontal',
                        'action'=>array("ListingsController@anyIndex"),
                        'method'=>'post',
                        'id'=>'adminForm'
                    )
            )}}
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.rent_sale')}}
				</p>
				<select class='elselect' name="pro_rent_sale">
					<option value="">{{trans('layout.any')}}</option>
					<option value="0">{{trans('layout.rent')}}
					</option>
					<option value="1">{{trans('layout.sale')}}
					</option>
				</select>
			</div>
			<div class="select-wrapper select-medium"><!--Vila/Flat/House-->
				<p>
					{{trans('layout.property_type')}}
				</p>
				<select class='elselect' name="pro_category_type">
						<option value="">{{trans('layout.any')}}</option>
                        @foreach($dataCategory as $key => $value)
                            <option value="{{$value['id']}}">
                            {{$value['name']}}
                            </option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-medium">
				<p>
					{{trans('layout.property_category')}}
				</p>
				<select class='elselect' name="pro_category"><!--other category-->
					<option value="">{{trans('layout.any')}}</option>
                    <option value="1">Commercial</option>
                    <option value="0">Residential</option>
				</select>
			</div>
			<div class="select-wrapper select-medium">
				<p>
					{{trans('layout.location')}}
				</p>
				<select class='elselect' name="province_id">
					<option value="">{{trans('layout.any')}}</option>
                        @foreach($locationArray as $key => $value)
                            <option value="{{$value['id']}}">
                            	{{$value['name']}}
                        	</option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.bedroom')}}
				</p>
				<select class='elselect' name="bedroom">
					<option value = ''>{{trans('layout.any')}}</option>
                        @foreach($room as $key => $value)
                        <option value='{{$key}}'>{{$value}}
                        </option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.bathroom')}}
				</p>
				<select class='elselect' name="bathroom">
					<option value = ''>{{trans('layout.any')}}</option>
                        @foreach($room as $key => $value)
                        <option value='{{$key}}'>
                            {{$value}}
                        </option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-small ">
				<p>
					{{trans('layout.min_price')}}
				</p>
				<input type="number" min="0" step="1" name="min_price" 
						placeholder="{{trans('layout.min_price')}}" value=""> 
			</div>
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.max_price')}}
				</p>
				<input type="number" min="0" step="1" name="max_price" 
					   value="" 
					   placeholder="{{trans('layout.min_price')}}"> 
			</div>
			<div class="select-wrapper select-medium">
				<button  class="yellow-btn" id="btn_save"> 
	                    {{trans('layout.search')}}
	            </button>
        	</div>
		</form>
	</div>
</div>
@stop
@section('content')
<div class="content-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 page-content">
				<div class="inner-wrapper">
					<div class="property-images-slider">
						<div id="details-slider" class="flexslider">
							<a href="#" class="yellow-btn-prize">$ 
							{{Helper::getNumberFormat($property['price'])}}
                             @if($property['type'])
                                {{''}}
                            @else
                                / pm
                            @endif</a>
							<a href="#" class='status'>
								@if($property['type'])
		                            {{trans('layout.for_sale')}}
                                @else
                                {{trans('layout.for_rent')}}
                                @endif
                            </a>
							<ul class="slides">
								<?php 
                                $strClassActive ='active';
                                 ?>
                                @foreach ($galleries as $i=>$gallery) 
                                <li>
                                <div class="image-wrapper">
									<img src="{{$gallery['path']}}" alt="gallery">
								</div>
								</li>
                                <?php 
                                $strClassActive = '';
                                 ?>
                                @endforeach
							</ul>
						</div>
						<div id="details-carousel" class="flexslider">
							<ul class="slides">
								<?php 
                                $strClassActive ='active';
                                 ?>
                                @foreach ($galleries as $i=>$gallery) 
                                <li>
									<img src="{{$gallery['path']}}" alt="gallery">
								</li>
                                <?php 
                                $strClassActive = '';
                                 ?>
                                </li>
                                @endforeach
								
							</ul>
						</div>
					</div>
					<div class="property-desc">
						<h3>{{$property['name']}}</h3>
						<ul class="slide-item-features item-features">
							<li><span class="fa fa-arrows-alt"></span>{{$property['land_area']}}
                            @if($property['land_area_unit'])
                            sq
                            @else
                            ha
                            @endif</li>
							<li><span class="fa fa-male"></span>{{$property['bathroom']}} bathrooms</li>
							<li><span class="fa fa-inbox"></span>{{$property['bedroom']}} bedrooms</li>
						</ul>
						<p class="first-paragraph">
							{{$property['description']}}
						</p>
						<div class="additional-features">
							<h3>{{trans('layout.add_feature')}}</h3>
							<ul class="features">
								<?php $count = 0;?>
							@if(count($featured))
								@foreach($featured as $key => $value)
								<?php $count++;?>
								<li class="single-feature"><a href="#">
									<i class="fa fa-check-circle"></i>{{$value['feature_name']}}</a>
								</li>
								@endforeach
							@endif
							@if($count == 0)
								<span class="no_record">{{trans('layout.no_record_found')}}</span>
							@endif
							</ul>
						</div>
						<div class="property-location">
							<h3>{{trans('layout.property_location')}}</h3>
							<div id="property-location-map">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 blog-sidebar">
				<div class="sidebar-widget author-profile">
					<h4 class="widget-title">{{trans('layout.list_by')}}</h4>
					<div class="image-box">
						<img src="{{$property['a_featured_image']}}" alt="agent">
						<ul class="social-icons">
							<li><a href="#" class="fa fa-google-plus"></a></li>
							<li><a href="#" class="fa fa-twitter"></a></li>
							<li><a href="#" class="fa fa-pinterest"></a></li>
							<li><a href="#" class="fa fa-dribbble"></a></li>
							<li><a href="#" class="fa fa-linkedin"></a></li>
							<li><a href="#" class="fa fa-facebook"></a></li>
						</ul>
					</div>
					<div class="desc-box">
						<h4>{{$property['a_first_name'].' '.$property['a_last_name']}}</h4>
						<p class="person-number">
							<i class="fa fa-phone"></i>(+855) {{Helper::getPhoneNumberFormat($property['a_phone1'])}}
						</p>
						@if(!is_null($property['a_phone2']))
						<p class="person-number">
							<i class="fa fa-phone"></i>(+855) {{Helper::getPhoneNumberFormat($property['a_phone2'])}}
						</p>
						@endif
						<p class="person-email">
							<i class="fa fa-envelope"></i>{{$property['a_email']}}
						</p>
						<a href="{{action('AgentController@getDetail',$property['primary_contact_id'])}}" class='gray-btn'>view full profile</a>
					</div>
				</div>
				<div class="sidebar-widget similar-listings-widget">
					<h4 class="widget-title">{{trans('layout.similar_listing')}}</h4>
					<ul class="similar-listings">
						<?php $count = 0; ?>
						@if(count($similars))
							@foreach($similars as $key => $value)
							<?php $count++;?>
						<li class="tab-content-item">
							<div class="pull-left thumb">
								<img src="{{$value['featured_image']}}" alt="thumbnail">
							</div>
							<h5><a href="{{action('ListingsController@getDetail',$value['id'])}}">{{Helper::getSubStr($value['name'])}} - $ {{$value['price']}}</a></h5>
						</li>
						@endforeach
						@endif
						
					</ul>
					@if($count == 0)
							<span class="no_record margin-top-20">{{trans('layout.no_record_found')}}</span>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('script')
@parent
<script type="text/javascript">
	var $img_marker = "{{asset('images/map-marker.png')}}";
	var map = null;
  	var marker = null;
  	var lat = <?php echo $property['latitude']?>;
  	var lng = <?php echo $property['longitude']?>;
  	function setMarker(latLng)
	{
 		if(marker==null)
 		{
 			marker = new google.maps.Marker({
 				map:map,
 				position:latLng,
 				title:'Company Location'
 			});
 			
 		}
 		else{
 			marker.setPosition(latLng);
 		}
 		map.setCenter(latLng);
	}
	function initmap()
  	{
  		var latlng = new google.maps.LatLng(lat, lng);
  	    if(map==null)
  		{
			var mapOptions = {
				zoom: 16,
				center: latlng
			};
		 	map = new google.maps.Map(document.getElementById('property-location-map'), mapOptions);
			
  		}	
  		setMarker(latlng);
  	}
	$(window).load(function() 
	{
  		initmap();
	});  
	var validateOption =
    {
       successClass: "has-success",
       errorClass: "has-error",
       classHandler: function (el) 
        {
            return el.$element.closest(".form-group");
       },
       errorsContainer: function (el) {
        return el.$element.closest(".form-group");
       },
       errorsWrapper: "<span class='help-block'></span>",
       errorTemplate: "<span></span>",
       trigger:"change",
    };
  	$('document').ready(function(){
  		 $('#submit-btn').click(function(e){

            var boolValid = true;
            var i = 0;
            var that = this;
            var lstControl = [];
            console.log($('.form-control').length);
            e.preventDefault();
            if ($('.form-control').length > 0) 
            {
                lstControl = $('.form-control').parsley(validateOption);
            }
            $.each(lstControl, function (inx, item) {
              item.validate();
              if (!item.isValid()) {
                  if (i == 0) {
                      item.$element.focus();
                  }
                  boolValid = false;
                  i++;
              }
            });
            if (!boolValid) {
              e.preventDefault();
            }
            else
            {
                getSentMail();
            }
        });
  	
  	});
  	function getSentMail()
    {
        $.ajax({
            url :'{{action("ContactController@postSentMail")}}',
            type : 'POST',
            data :
            {
              name      : $('#name').val(),
              email     : $('#email').val(),
              website   : $('#website').val(),
              message   : $('#message').val(),
            },
            beforeSend:function()
            {
                $("#loading").show();
            },
            success:function(data)
            {
              $('#loading').hide();
              if(data.success == true)
              {               
                $('.alert-danger').hide();
                $('.alert-success').show();
                $('.alert-success').html(data.message);
              }
              else
              {
                $('.alert-success').hide();
                $('.alert-danger').show();
                $('.alert-danger').html(data.message);
              }
            }
        });
    }
</script>
@stop




