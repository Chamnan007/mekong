@extends('layout.default')

@section('slider')
<div class="page-title-section">
		<div class="container">
			<div class="pull-left page-title">
				<a href="#">
				<h2>{{trans('layout.contact_us')}}</h2>
				</a>
			</div>
			<div class="pull-right breadcrumb">
				<a href="{{action('HomeController@anyIndex')}}">{{trans('layout.home')}}</a><span class="fa fa-arrow-circle-right sep"></span><a>{{trans('layout.contact')}}</a>
			</div>
		</div>
</div>
@stop

@section('search_section')
<?php 
	$dataCategory = array();
	$locationArray = array();
	if(isset($datacategoryType['data']))
    {
    	$dataCategory = $datacategoryType['data'];
    }
    if(isset($location['data']))
    {
    	$locationArray = $location['data'];
    }
?>
<div class="search-section">
	<div class="container">
		{{Form::open(
                    array(
                        'class'=>'form-horizontal',
                        'action'=>array("ListingsController@anyIndex"),
                        'method'=>'post',
                        'id'=>'adminForm'
                    )
            )}}
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.rent_sale')}}
				</p>
				<select class='elselect' name="pro_rent_sale">
					<option value="">{{trans('layout.any')}}</option>
					<option value="0">{{trans('layout.rent')}}
					</option>
					<option value="1">{{trans('layout.sale')}}
					</option>
				</select>
			</div>
			<div class="select-wrapper select-medium"><!--Vila/Flat/House-->
				<p>
					{{trans('layout.property_type')}}
				</p>
				<select class='elselect' name="pro_category_type">
						<option value="">{{trans('layout.any')}}</option>
                        @foreach($dataCategory as $key => $value)
                            <option value="{{$value['id']}}">
                            {{$value['name']}}
                            </option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-medium">
				<p>
					{{trans('layout.property_category')}}
				</p>
				<select class='elselect' name="pro_category"><!--other category-->
					<option value="">{{trans('layout.any')}}</option>
                    <option value="1">Commercial</option>
                    <option value="0">Residential</option>
				</select>
			</div>
			<div class="select-wrapper select-medium">
				<p>
					{{trans('layout.location')}}
				</p>
				<select class='elselect' name="province_id">
					<option value="">{{trans('layout.any')}}</option>
                        @foreach($locationArray as $key => $value)
                            <option value="{{$value['id']}}">
                            	{{$value['name']}}
                        	</option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.bedroom')}}
				</p>
				<select class='elselect' name="bedroom">
					<option value = ''>{{trans('layout.any')}}</option>
                        @foreach($room as $key => $value)
                        <option value='{{$key}}'>{{$value}}
                        </option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.bathroom')}}
				</p>
				<select class='elselect' name="bathroom">
					<option value = ''>{{trans('layout.any')}}</option>
                        @foreach($room as $key => $value)
                        <option value='{{$key}}'>
                            {{$value}}
                        </option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-small ">
				<p>
					{{trans('layout.min_price')}}
				</p>
				<input type="number" min="0" step="1" name="min_price" 
						placeholder="{{trans('layout.min_price')}}" value=""> 
			</div>
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.max_price')}}
				</p>
				<input type="number" min="0" step="1" name="max_price" 
					   value="" 
					   placeholder="{{trans('layout.min_price')}}"> 
			</div>
			<div class="select-wrapper select-medium">
				<button  class="yellow-btn" id="btn_save"> 
	                    {{trans('layout.search')}}
	            </button>
        	</div>
		</form>
	</div>
</div>
@stop
<!-- content-Section -->
@section('content')

<div class="content-section">
	<div class="container">
		<div class="col-md-12 map-wrapper no-padding">
			<div class="inner-wrapper">
				<div id="map">
				</div>
				<div class="clearfix">
				</div>
			</div>
		</div>
		<div class="col-md-6 contact-info no-padding-left">
			<div class="inner-wrapper">
				<h4 class="box-title">{{trans('layout.contact_info')}}</h4>
				<p class='first-paragraph'>
					{{$datacompanyProfile['data']['description']}}
				</p>
				<div class="info-wrapper">
					<span class="pull-left"><i class="fa fa-home"></i>{{$datacompanyProfile['data']['address']}}</span>
					<span class="pull-left"><i class="fa fa-phone"></i>(+855) {{Helper::getPhoneNumberFormat($datacompanyProfile['data']['phone_1'])}}</span>
					@if(!is_null($datacompanyProfile['data']['phone_2']))
					<span class="pull-left"><i class="fa fa-phone"></i>(+855) {{Helper::getPhoneNumberFormat($datacompanyProfile['data']['phone_2'])}}</span>
					@endif
					<span class="pull-left"><i class="fa fa-envelope"></i>{{$datacompanyProfile['data']['email']}}</span>
				</div>
				<div class="clearfix">
				</div>
			</div>
		</div>
		<div class="col-md-6 contact-form-wrapper no-padding-right">
			<div class="inner-wrapper">
				<h4 class="box-title">{{trans('layout.get_in_touch')}}</h4>
				<form class='contact-form' method="POST">
					<div class="contact-form-left">
						
						<div class="form-group margin-bottom-0">
							<span><i class='fa fa-user'></i></span>
							<input type="text" name='name' placeholder='Name' id='name'  
								   data-parsley-required="true"
	                               maxlength="50" 
	                               data-parsley-maxlength="50">
						</div>
						<div class="form-group margin-bottom-0">						
							<span><i class='fa fa-envelope-o'></i></span>
						
							<input type="email" name='email' placeholder='e-mail' id='email' 
						           data-parsley-required="true"
	                               maxlength="50" 
	                               data-parsley-maxlength="50">
	                    </div>
	                    <div class="form-group margin-bottom-0">
							<span><i class='fa fa-link'></i></span>
							<input type="url" name='website' placeholder='website' 
								id='website'
		                        maxlength="100" 
		                        data-parsley-maxlength="100"
								>
						</div>
					</div>
					<div class="contact-form-right">
						<div class="form-group margin-bottom-0">
							<textarea id="message" name='message' placeholder='Message' 
								 maxlength="500" data-parsley-maxlength="500"></textarea>
						</div>
						<button type="button" id='submit-btn'>SENT MESSAGE <img src="{{asset('img/loading.gif')}}" style="display:none" id="loading"></button>
					
					</div>
				</form>
				<div class="clearfix"></div>
				<div class="alert alert-success" style="display:none"></div>
                <div class="alert alert-danger" style="display:none"></div>
				<div class="clearfix">
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('script')
@parent
<script type="text/javascript">
	var $img_marker = "{{asset('images/map-marker.png')}}";
	var map = null;
  	var marker = null;
  	var lat = <?php echo $datacompanyProfile['data']['latitude']?>;
  	var lng = <?php echo $datacompanyProfile['data']['longitude']?>;
  	function setMarker(latLng)
	{
 		if(marker==null)
 		{
 			marker = new google.maps.Marker({
 				map:map,
 				position:latLng,
 				title:'Company Location'
 			});
 			
 		}
 		else{
 			marker.setPosition(latLng);
 		}
 		map.setCenter(latLng);
	}
	function initmap()
  	{
  		var latlng = new google.maps.LatLng(lat, lng);
  	    if(map==null)
  		{
			var mapOptions = {
				zoom: 16,
				center: latlng
			};
		 	map = new google.maps.Map(document.getElementById('map'), mapOptions);
			
  		}	
  		setMarker(latlng);
  	}
	$(window).load(function() 
	{
  		initmap();
	});  
  	$('document').ready(function(){
  		  $("#submit-btn").click(function () {
	        	var has_error = 0 ;
	            var name = $("#name").val();
	            var message = $("#message").val();
	            var email = $("#email").val();
	            var website = $("#website").val();
	            var atpos = email.indexOf("@");
	            var dotpos = email.lastIndexOf(".");
	            var dataString = '&name=' + name + '&email=' + email + '&website=' + website + '&message=' + message;

	            $('input[type=text]').focus(function () {
	                $(this).css({
	                    "background-color": "#fff"
	                });
	            });
	            $('textarea').focus(function () {
	                $(this).css({
	                    "background-color": "#fff"
	                });
	            });

	            if ($("#name").val().length == 0) {
	           		has_error = 1 ;
	                $('#name').css({
	                    "background-color": "rgba(248, 116, 116, 0.52)"
	                });
	            }
	            if($("#email").val().length == 0) {
	            has_error = 1 ;
	                $('#email').css({
	                    "background-color": "rgba(248, 116, 116, 0.52)"
	                });
	            }
	            if(atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
	            has_error = 1 ;
	                $('#email').css({
	                    "background-color": "rgba(248, 116, 116, 0.52)"
	                });
	            }
	            if($("#message").val().length == 0) {
	            has_error = 1 ;
	                $('#message').css({
	                    "background-color": "rgba(248, 116, 116, 0.52)"
	                });
	            } 
	            if(has_error == 0 ) {
	               getSentMail();
	            }
	            return false;
	        });
  	
  	});
  	function getSentMail()
    {
        $.ajax({
            url :'{{action("ContactController@postSentMail")}}',
            type : 'POST',
            data :
            {
              name      : $('#name').val(),
              email     : $('#email').val(),
              website   : $('#website').val(),
              message   : $('#message').val(),
            },
            beforeSend:function()
            {
                $("#loading").show();
            },
            success:function(data)
            {
              $('#loading').hide();
              if(data.success == true)
              {               
                $('.alert-danger').hide();
                $('.alert-success').show();
                $('.alert-success').html(data.message);
              }
              else
              {
                $('.alert-success').hide();
                $('.alert-danger').show();
                $('.alert-danger').html(data.message);
              }
            }
        });
    }
</script>
@stop



