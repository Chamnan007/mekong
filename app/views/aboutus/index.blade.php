@extends('layout.default')

@section('slider')
<div class="page-title-section">
		<div class="container">
			<div class="pull-left page-title">
				<a href="#">
				<h2>property listing</h2>
				</a>
			</div>
			<div class="pull-right breadcrumb">
				<a href="{{action('HomeController@anyIndex')}}">home</a><span class="fa fa-arrow-circle-right sep"></span><a>about us</a>
			</div>
		</div>
</div>
@stop

@section('search_section')

@stop

<!-- content-Section -->
@section('content')

<?php 

$dataUrgent = $dataPropertyUrgent['data'];
$dataRecent = $dataPropertyRecent['data'];

?>

<div class="content-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 blog-content">
					<div class="post-content">
						<h2>{{$dataAboutUs['branch_name']}}</h2>
						{{$dataAboutUs['about_us']}}
						<h2>{{"Our Services"}}</h2>
						@foreach( $dataService as $key => $value )
							<h3 style="font-weight: bold;">{{$value['title']}}</h3>
							{{$value['description']}}
						@endforeach
					</div>
				<div class="clearfix">
				</div>
			</div>
			<div class="col-md-4 blog-sidebar">
				<div class="sidebar-widget tabbed-content">
					<ul class="nav nav-tabs" id="myTab">
						<li class="active"><a href="#popular" data-toggle="tab">featured</a></li>
						<li class=""><a href="#recent" data-toggle="tab">recent</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade active in" id="popular">
							<ul class="tab-content-wrapper">
								@foreach( $dataUrgent as $uKey => $uValue )
									<li class="tab-content-item">
										<div class="pull-left thumb">
											<img src="{{@$uValue['property']['featured_image']}}">
										</div>
										<h5>
											<a href="#">{{$uValue['property']['name'] . " - $ " . Helper::getNumberFormat($uValue['property']['price'])}}</a>
										</h5>
									</li>
								@endforeach
							</ul>
						</div>
						<div class="tab-pane fade" id="recent">
							<ul class="tab-content-wrapper">
								@foreach( $dataRecent as $rKey => $rValue )
									<li class="tab-content-item">
										<div class="pull-left thumb">
											<img src="{{@$rValue['property']['featured_image']}}">
										</div>
										<h5>
											<a href="#">{{$rValue['property']['name'] . " - $ " . Helper::getNumberFormat($rValue['property']['price'])}}</a>
										</h5>
									</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop