@extends('layout.default')
@section('title','Home Page')
@section('header','Property Homepage in Cambodia')
@section('slider')
<?php 

$properties = array();
$slideshow = array();
$dataCategory = array();
$locationArray = array();
if(isset($dataProperty['data']))
{
    $properties = $dataProperty['data']; 
}
if(isset($dataSlideshow['data']))
{
   $slideshow = $dataSlideshow['data'];
}
if(isset($datacategoryType['data']))
{
	$dataCategory = $datacategoryType['data'];
}
if(isset($location['data']))
{
	$locationArray = $location['data'];
}

?>
<div class="slider-section">
	<div class="main-flexslider">
		<ul class="slides">
			<?php $count = 0?>
			@foreach($slideshow as $key=> $slide)
			<?php $count++;?>
			<li class='slides' id='{{$count}}'><img src="{{$slide['featured_image_1970_750']}}" alt="slide 01">
			<!-- <div class="slide-box">
				<h2>{{$slide['caption']}}</h2>
				<p>
					 {{$slide['description']}}
				</p>				
			</div> -->
			</li>
			@endforeach
		</ul>
	</div>
</div>
@stop
@section('search_section')
<div class="search-section">
	<div class="container">
		{{Form::open(
                    array(
                        'class'=>'form-horizontal',
                        'action'=>array("ListingsController@anyIndex"),
                        'method'=>'post',
                        'id'=>'adminForm'
                    )
            )}}
			<div class="select-wrapper select-big">
				<p>
					{{trans('layout.rent_sale')}}
				</p>
				<select class='elselect' name="pro_rent_sale">
					<option value="">any</option>
					<option value="0">{{trans('layout.rent')}}</option>
					<option value="1">{{trans('layout.sale')}}</option>
				</select>
			</div>
			<div class="select-wrapper select-big"><!--Vila/Flat/House-->
				<p>
					{{trans('layout.property_type')}}
				</p>
				<select class='elselect' name="pro_category_type">
						<option value="">{{trans('layout.any')}}</option>
                        @foreach($dataCategory as $key => $value)
                            <option value="{{$value['id']}}">{{$value['name']}}</option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-big">
				<p>
					{{trans('layout.property_category')}}
				</p>
				<select class='elselect' name="pro_category"><!--other category-->
					<option value="">any</option>
                    <option value="1">Commercial</option>
                    <option value="0">Residential</option>
				</select>
			</div>
			<div class="select-wrapper select-big">
				<p>
					{{trans('layout.location')}}
				</p>
				<select class='elselect' name="province_id">
					<option value="">{{trans('layout.any')}}</option>
                        @foreach($locationArray as $key => $value)
                            <option value="{{$value['id']}}">{{$value['name']}}</option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-medium">
				<button  class="yellow-btn" id="btn_save"> 
	                    {{trans('layout.search')}}
	            </button>
        	</div> 

			<!-- 
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.bedroom')}}
				</p>
				<select class='elselect' name="bedroom">
					<option value = ''>{{trans('layout.any')}}</option>
                        @foreach($room as $key => $value)
                        <option value='{{$key}}'>{{$value}}</option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.bathroom')}}
				</p>
				<select class='elselect' name="bathroom">
					<option value = ''>{{trans('layout.any')}}</option>
                        @foreach($room as $key => $value)
                        <option value='{{$key}}'>{{$value}}</option>
                        @endforeach
				</select>
			</div>
			<div class="select-wrapper select-small ">
				<p>
					{{trans('layout.min_price')}}
				</p>
				<input type="number" min="0" step="1" name="min_price" placeholder="{{trans('layout.min_price')}}"> 
			</div>
			<div class="select-wrapper select-small">
				<p>
					{{trans('layout.max_price')}}
				</p>
				<input type="number" min="0" step="1" name="max_price" placeholder="{{trans('layout.max_price')}}"> 
			</div>
			-->
		</form>
	</div>
</div>
@stop
@section('content')
<div class="content-section">
	<div class="recent-listings">
	<div class="container">
		<div class="title-box">
			<h3>{{trans('layout.recent_listings')}}</h3>
			<div class="bordered">
			</div>
		</div>
		<div class="row listings-items-wrapper" id="property-container">
			<?php $count=0;?>
			@foreach ($properties as $i=>$pro)                      
            <?php $count++; 
                $property = array();
                if(isset($pro['property']))
                {
                    $property = $pro['property'];
                }
            ?> 
			<div class="col-md-4 listing-single-item">
				<div class="item-inner">
					<div class="image-wrapper">
						<img src="{{$property['featured_image']}}" alt="Featured Image">
						@if($property['is_urgent'])
						<a href="#" class='featured'><i class='fa fa-star'></i>featured</a>
						@endif
					</div>
					<div class="desc-box">
						<h4><a href="{{action('ListingsController@getDetail',$property['id'])}}">{{Helper::getSubStr($property['name'])}}</a></h4>
						<ul class="slide-item-features item-features">
							<li><span class="fa fa-arrows-alt"></span>
								{{$property['land_area']}}
								@if($property['land_area_unit'])
                                sq
                                @else
                                ha
                                @endif</li>
							<li><span class="fa fa-male"></span>{{$property['bathroom']}} bathrooms</li>
							<li><span class="fa fa-inbox"></span>{{$property['bedroom']}}bedrooms</li>
						</ul>
						<div class="buttons-wrapper">
							<a href="#" class="yellow-btn">
								&dollar;{{Helper::getNumberFormat($property['price'])}}
                                @if($property['type'])
                                    {{''}}
                                @else
                                    / pm
                                @endif</a>
							<a href="{{action('ListingsController@getDetail',$property['id'])}}" class="gray-btn"><span class="fa fa-file-text-o"></span>Details</a>
						</div>
						<div class="clearfix">
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
		@if($count == 0)
			<span class="no_record">{{trans('layout.no_record_found')}}</span>
		@elseif($count > 6)
		<div class="col-sm-12 load-more-container">
			<a href="#" class="gray-btn load-more" id="load-more">Load More</a>
		</div>
		@endif
	</div>
</div>
</div>
<style type="text/css">
.load-more-container
{
	margin-top: 40px;
	text-align: center;
}
.recent-listings .container .listings-items-wrapper
{
	width: 100%;
}
</style>
@stop
@section('script')
@parent
<script type="text/javascript">
  	$('document').ready(function(){
  		var page = 1;
  		$('#load-more').click(function(event){
  			event.preventDefault();
  			page++;
  			getMore(page)
  		});
  	});
  	function getMore(page)
  	{
  		$.ajax({
  			url : '{{action("HomeController@postMore")}}',
  			type : 'POST',
  			data : { page : page },
  			beforeSend:function()
  			{
  			},
  			success:function(data)
  			{
  				var count = data.data,
  					str = data.str;
  				$('#property-container').append(str);
  				// hide load more button if no more property
  				if(count < 6)
  				{
  					$('#load-more').fadeOut();
  				}
  			}
  		});
  	}
</script>
@stop