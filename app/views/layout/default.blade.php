<!doctype html>
<html lang="en-us">
<head>
<!--Page Title-->
<title>Century 21 - Mekong</title>
<!--Meta Tags-->
<meta charset="UTF-8">
<meta name="author" content="">
<meta name="keywords" content=""/>
<meta name="description" content="Propety Agency In Cambodia, Popular Agency, Property"/>
<!-- Set Viewport-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{asset('css/bootstrap-theme.min.css')}}" type="text/css"/>
<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('css/flexslider.css')}}">
<link rel="stylesheet" href="{{asset('css/select-theme-default.css')}}">
<link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
<link rel="stylesheet" href="{{asset('css/owl.theme.css')}}">
<link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css"/>
<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body id='top' class="blog-page single-page">
<header>
	@include('layout.header')
</header>

	@yield('slider')

<!-- /Header -->

	@yield('search_section')

<!-- home -->
	@yield('content')

<!-- Search-Section -->

<!-- footer-section -->
@include('layout.footer')


<!-- Javascript -->
<script type="text/javascript" src="{{asset('js/jquery-2.1.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.flexslider-min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/select.min.js')}}"></script>
<script src="{{asset('js/parsley.min.js')}}" type='text/javascript'></script> 
<script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="{{asset('js/jquery.unveil.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/script.js')}}"></script>

<!-- google analytic -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66236237-1', 'auto');
  ga('send', 'pageview');

</script>

	@yield('script') 

</body>
</html>