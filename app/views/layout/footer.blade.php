<footer>
<div class="container">
	<?php
                $param['branch_id'] = Config::get('global.branch_id',8);
                $strParm = Helper::buildParam($param);
                $url    = Config::get('global.base_api_url');
                $url    .=  'profile?';
                $url    .=  $strParm;
                $companyProfile = RestClient::get($url)->getContent();
                $datacompanyProfile = json_decode($companyProfile,true);

                $profile = array();
                if(isset($datacompanyProfile['data']))
                {
                    $profile = $datacompanyProfile['data']; 
                }
    ?>
	<div class="col-md-4 footer-recent-posts no-padding-left">
		<a class="logo" href="#"><img src="{{@$profile['logo']}}" alt="logo"></a>
		<p>
			{{@$profile['description']}}
		</p>
	</div>
	<div class="col-md-4 footer-recent-posts">
		<h3 class="footer-title">Quick Links</h3>
		<ul>
			<li><a href="{{action('HomeController@anyIndex')}}"><i class="fa fa-arrow-circle-right"></i>{{trans('layout.home')}}</a></li>
			<li><a href="{{action('ListingsController@anyIndex')}}"><i class="fa fa-arrow-circle-right"></i>{{trans('layout.listing')}}</a></li>
			<li><a href="{{action('ContactController@anyIndex')}}"><i class="fa fa-arrow-circle-right"></i>{{trans('layout.contact_us')}}</a></li>
			<li><a href="{{action('AboutUsController@anyIndex')}}"><i class="fa fa-arrow-circle-right"></i>{{trans('layout.about_us')}}</a></li>
		</ul>
	</div>
	<div class="col-md-4 footer-contact-info">
		<h3 class='footer-title'>{{trans('layout.contact_info')}}</h3>
		<p class="website-number col-sm-12 no-padding">
			<i class="fa fa-phone"></i> 
			(+855) {{Helper::getPhoneNumberFormat(@$profile['phone_1'])}}
		</p>
		@if(!is_null(@$profile['phone_2']))
		<p class="website-number col-sm-12 no-padding">
			<i class="fa fa-phone"></i> 
			(+855) {{Helper::getPhoneNumberFormat(@$profile['phone_2'])}}
		</p>
		@endif
		<p class="website-email col-sm-12 no-padding">
			<i class="fa fa-envelope"></i> {{@$profile['email']}}
		</p>
	</div>
</div>
</footer>
<div class="bottom-strip">
	<div class="container">
		<div class="col-md-5 no-padding">
			<p class='pull-left'>
				<p>&copy; 2015 CENTURY 21 Imperial Realty. All Rights Reserved.</p>
				<p>Each CENTURY 21 Office is Independently Owned And Operated.</p>
			</p>
		</div>
		<div class="col-md-4 bottom-strip-middle">
			<a href="#top" class='fa fa-arrow-circle-up' id='backtop-btn'></a>
		</div>
		<div class="col-md-3">
			<ul class="social-icons">
				     <li><a href="#" class="fa fa-google-plus"></a></li>
				     <li><a href="#" class="fa fa-twitter"></a></li>
				     <li><a href="#" class="fa fa-pinterest"></a></li>
					 <li><a href="#" class="fa fa-dribbble"></a></li>
					 <li><a href="#" class="fa fa-linkedin"></a></li>
				<li><a href="#" class="fa fa-facebook"></a></li>
			</ul>
		</div>
	</div>
</div>