<!-- /Header -->
	<?php
		// for token
		$app_id = APP_ID;
		$token = Helper::generateToken(APP_ID);
		
		$param['branch_id'] = Config::get('global.branch_id',8);
        $strParm = Helper::buildParam($param);
        $url    = Config::get('global.base_api_url');
        $url    .=  'profile?';
        $url    .=  $strParm;
        $companyProfile = RestClient::get($url)->getContent();
        $datacompanyProfile = json_decode($companyProfile,true);

        $profile = array();
        if(isset($datacompanyProfile['data']))
        {
            $profile = $datacompanyProfile['data']; 
        }
	?>
	<div id="premium-bar">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="{{action('HomeController@anyIndex')}}"><img src="{{@$profile['logo']}}" alt="logo"></a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li class="{{Helper::getMenuActiveClass('HomeController@anyIndex')}}">
		                    <a href="{{action('HomeController@anyIndex')}}">{{trans('layout.home')}}</a>
		                </li>
						<li class="dropdown {{Helper::getMenuActiveClass('ListingsController@anyIndex')}}">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{trans('layout.listing')}} <b class="caret"></b></a>
							<ul class="dropdown-menu" style="display: none;">
								<li><a href="{{action('ListingsController@anyRent')}}">{{trans('layout.rent')}}</a></li>
								<li><a href="{{action('ListingsController@anySale')}}">{{trans('layout.sale')}}</a></li>
							</ul>
						</li>
						<li class="{{Helper::getMenuActiveClass('NewsController@anyIndex')}}">
							<a href="{{action('NewsController@anyIndex')}}">{{trans('layout.news')}}</a>
						</li>
						<!-- <li class="{{Helper::getMenuActiveClass('AboutUsController@anyIndex')}}">
							<a href="{{action('AboutUsController@anyIndex')}}">{{trans('layout.about_us')}}</a>
						</li> -->
						<li class="dropdown {{Helper::getMenuActiveClass('AboutUsController@anyIndex')}}">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{trans('layout.about_us')}} <b class="caret"></b></a>
							<ul class="dropdown-menu" style="display: none;">
								<li><a href="{{action('AboutUsController@anyIndex')}}">{{trans('layout.company_profile')}}</a></li>
								<li><a href="{{action('AgentController@anyIndex')}}">{{trans('layout.our_agent')}}</a></li>
							</ul>
						</li>
						<li class="{{Helper::getMenuActiveClass('ContactController@anyIndex')}}">
							<a href="{{action('ContactController@anyIndex')}}">{{trans('layout.contact_us')}}</a>
						</li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
			</nav>
		</div>
	</div>
