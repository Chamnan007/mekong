<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/','HomeController@anyIndex');
Route::controller('home','HomeController');
Route::controller('listings','ListingsController');
Route::controller('contact', 'ContactController');
Route::controller('aboutus', 'AboutUsController');
Route::controller('agent', 'AgentController');
Route::controller('news', 'NewsController');